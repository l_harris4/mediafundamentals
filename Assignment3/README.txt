------Please run the application from visual studio in debug mode.

Use the platform toolset 141

Use Multi-Byte Character set

Use Visual Studio 2017

	A progress bar is shown in the output window. The user moves the story forward by pressing the right
arrow key. All the sounds for the current scene are displayed. Also all the attributes for a single sound
are displayed. The user can also use the left arrow key to go backward in the story.
	The user can edit the sounds in the scene in a few ways.

Controls:
Up and Down arrow keys to change which sound is being displayed.
"w" and "s" to cycle through which attribute of the sound will be changed.
"a" and "d" to actually affect the current attribute.
"q" to switch between changing all sounds or one specific one.

	The way playback speed was implemented was by simply changing the frequency. This was because using 
fmod method to change playback speed didn't work with all sound formats, and I wanted to support all formats.
 The options for playback speed are half speed (0.5), full speed (1.0), and finally double speed (2.0).