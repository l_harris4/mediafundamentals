#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>

#include "MediaObject.h"
#include <stdio.h>

#include <Windows.h>
#include <sapi.h>
#include <sphelper.h>
#include "utils.h"

#define STREAM_BUFFER_SIZE 65536
#define NUMBER_OF_TAGS 4
#define DEVICE_INDEX    0

using namespace std;

//global variables
FMOD::System *g_psystem = NULL;
FMOD_RESULT g_result;
HRESULT hr;
HRESULT hr2;
CComPtr<ISpVoice> cpVoice;
CComPtr<ISpStream> cpStream;
CSpStreamFormat	cAudioFormat;
bool recording = false;

bool g_keydown = false;
bool g_mis_esc = false;
std::vector<MediaObject> mediaObjects;
std::vector<std::string> readingText;
std::vector<std::string> readingText2;
FMOD::ChannelGroup *group_a, *group_b, *group_master;
FMOD::DSP *dspecho = 0;
FMOD::DSP *dspreverb = 0;
FMOD::DSP *dsplowpass = 0;

FMOD::DSP *dspdistortion = 0;
FMOD::DSP *dsppitchshift = 0;
FMOD::DSP *dsptremolo = 0;

//net radio stations
char *murl_80s_1 = "http://193.200.43.23:80";
char *murl_80s_2 = "http://109.169.26.139:8432";
char *murl_80s_3 = "http://54.37.64.133:80";

FMOD_OPENSTATE mopenstate = FMOD_OPENSTATE_READY;
int mtag_index = 0;
char mtag_string[NUMBER_OF_TAGS][128] = { 0 };

//TODO: Create voice object.
ISpVoice *pVoice = NULL;

string g_baseSoundFilePath = "../Sounds/";

enum eChangingAttribute {
	VOLUME = 0,
	BALANCE,
	PITCH,
	PLAYBACKSPEED,
	FREQUENCY
};

eChangingAttribute changingAttribute = eChangingAttribute::VOLUME;

//a method to check if an error has occured
void checkForErrors() {
	if (::g_result != FMOD_OK) {
		fprintf(stderr, "FMOD error! (%d) %s\n", ::g_result, FMOD_ErrorString(::g_result));
		exit(-1);
	}
}

//for initliazing fmod
void init_fmod() {

	// Create the main system object.
	::g_result = FMOD::System_Create(&::g_psystem);
	checkForErrors();

	//Initializes the system object, and the msound device. This has to be called at the start of the user's program
	::g_result = ::g_psystem->init(512, FMOD_INIT_NORMAL, 0);    // Initialize FMOD.
	checkForErrors();

}

//a custom compare used later for sorting a vector of pairs
bool pairCompare(const pair<int, int>& firstElem, const pair<int, int>& secondElem) {
	return firstElem.second < secondElem.second;
}

//converting the code into a readable string
string convertSoundFormatCode(int code) {
	switch (code) {
	case 0:
		return " NONE";
		break;
	case 1:
		return " PCM8";
		break;
	case 2:
		return " PCM16";
		break;
	case 3:
		return " PCM24";
		break;
	case 4:
		return " PCM32";
		break;
	case 5:
		return " PCMFLOAT";
		break;
	case 6:
		return " BITSTREAM";
		break;
	case 7:
		return " MAX";
		break;
	case 65536:
		return " FORCEINT";
		break;
	}
}

//converting the code into a readable string
string convertSoundTypeCode(int code) {

	switch (code) {
	case 0:
		return " UNKNOWN";
		break;
	case 1:
		return " AIFF";
		break;
	case 2:
		return " ASF";
		break;
	case 3:
		return " DLS";
		break;
	case 4:
		return " FLAC";
		break;
	case 5:
		return " FSB";
		break;
	case 6:
		return " IT";
		break;
	case 7:
		return " MIDI";
		break;
	case 8:
		return " MOD";
		break;
	case 9:
		return " MPEG";
		break;
	case 10:
		return " OGGVORBIS";
		break;
	case 11:
		return " PLAYLIST";
		break;
	case 12:
		return " RAW";
		break;
	case 13:
		return " S3M";
		break;
	case 14:
		return " USER";
		break;
	case 15:
		return " WAV";
		break;
	case 16:
		return " XM";
		break;
	case 17:
		return " XMA";
		break;
	case 18:
		return " AUDIOQUEUE";
		break;
	case 19:
		return " AT9";
		break;
	case 20:
		return " VORBIS";
		break;
	case 21:
		return " MEDIA_FOUNDATION";
		break;
	case 22:
		return " MEDIACODEC";
		break;
	case 23:
		return " FADPCM";
		break;
	case 24:
		return " MAX";
		break;
	}
}

//a method to load all the data in from a file
bool LoadSoundsFromFileToWav()
{
	string filename = "ReadText2.txt";
	string line;
	ifstream inFile(filename.c_str());
	MediaObject tempMediaObject;
	bool inScene = false;
	bool inSample = false;

	try {
		while (getline(inFile, line))
		{
			readingText2.push_back(line);
		}
	}
	catch (...)
	{
		return false;
	}
	

	//TODO: set audio format	
	if (SUCCEEDED(hr2)) {
		hr2 = cAudioFormat.AssignFormat(SPSF_22kHz16BitStereo);
	}

	//create the folder if it doesnt exist
	if (CreateDirectory("c:\\harris_luke", NULL) ||
		ERROR_ALREADY_EXISTS == GetLastError())
	{

		//TODO: bind stream to file
		if (SUCCEEDED(hr2)) {
			hr2 = SPBindToFile(L"c:\\harris_luke\\test.wav", SPFM_CREATE_ALWAYS, &cpStream, &cAudioFormat.FormatId(), cAudioFormat.WaveFormatExPtr());
		}

		//TODO: set output to cpstream
		if (SUCCEEDED(hr2)) {
			hr2 = cpVoice->SetOutput(cpStream, true);
		}

		//TODO: speak
		if (SUCCEEDED(hr2)) {
			for (int i = 0; i < readingText2.size(); ++i)
			{
				std::wstring stemp = std::wstring(readingText[i].begin(), readingText[i].end());
				hr2 = cpVoice->Speak(stemp.c_str(), SPF_DEFAULT, NULL);
			}
		}

		//TODO: close the stream
		if (SUCCEEDED(hr2)) {
			hr2 = cpStream->Close();
		}



		FMOD::Sound* sound1;
		FMOD::Channel* channel1 = 0;
		::g_result = ::g_psystem->createSound("c:\\harris_luke\\test.wav", FMOD_CREATESTREAM, 0, &sound1);
		checkForErrors();

		::g_result = sound1->setMode(FMOD_LOOP_NORMAL);
		checkForErrors();

		::g_result = ::g_psystem->playSound(sound1, 0, false, &channel1);
		checkForErrors();

		MediaObject media;
		//assign sound and channel to the temp media object
		media.sound = sound1;
		media.channel = channel1;
		media.streamObject = true;
		media.fileName = line;

		media.channel->setChannelGroup(group_b);
		//put the media object on the backest scene object
		mediaObjects.push_back(media);
	}

	return true;
}

//a method to load all the data in from a file
bool LoadSoundsFromFile()
{
	string filename = "ReadText.txt";
	string line;
	ifstream inFile(filename.c_str());
	MediaObject tempMediaObject;
	bool inScene = false;
	bool inSample = false;

	try {
		while (getline(inFile, line))
		{
			readingText.push_back(line);
		}
	}
	catch (...)
	{
		return false;
	}
	return true;
}

void handle_keyboard() {

	//Esc key pressed
	if (GetAsyncKeyState(VK_ESCAPE)) {
		::g_mis_esc = true;
	}

	//===============================================================================================
	//Arrow UP
	else if ((GetKeyState(VK_UP) < 0) && !::g_keydown) {
		::g_keydown = true;
		LoadSoundsFromFileToWav();
		Sleep(200);
		::g_keydown = false;
	}

	//Arrow Down
	else if ((GetKeyState(VK_DOWN) < 0) && !::g_keydown) {
		::g_keydown = true;
		readingText[0] = "<voice required='Gender = Female'>" + readingText[0] + "</voice>";
		readingText[1] = "<voice required='Gender = Female'><spell>" + readingText[1] + "</spell></voice>";
		//readingText[2] = "<voice required='Gender = Female'><emph>" + readingText[2] + "</emph></voice>";
		readingText[2] = "<voice required='Gender = Female'><emph>" +
			readingText[2].substr(0, readingText[2].size() / 2) +
			"</emph>" + readingText[2].substr(readingText[2].size() / 2, readingText[2].size() / 2) +
			"</voice>";
		readingText[3] = "<rate speed='-5'>" + readingText[3] + "</rate>";
		readingText[4] = "<rate speed='10'>" + readingText[4] + "</rate>";
		readingText[5] = "<rate speed='15'>" + readingText[5] + "</rate>";
		readingText[6] = "<voice required='Gender = Female'>" + readingText[6].substr(0, readingText[6].size() / 2) + "<silence msec=\"800\"/>" + readingText[6].substr(readingText[6].size() / 2, readingText[6].size() / 2) + "</voice>";
		readingText[7] = "<voice required='Gender = Female'><pitch absmiddle='5'>" + readingText[7] + "</pitch></voice>";
		readingText[8] = "<voice required='Gender = Female'><pitch absmiddle='10'>" + readingText[8] + "</pitch></voice>";

		for (int i = 0; i < 9; ++i)
		{
			std::wstring stemp = std::wstring(readingText[i].begin(), readingText[i].end());
			hr = pVoice->Speak(stemp.c_str(), 0, NULL);
		}

		Sleep(200);
		::g_keydown = false;
	}

	//===============================================================================================
	//Arrow right  //Pressing this will further the scene
	else if ((GetKeyState(VK_RIGHT) < 0) && !::g_keydown) {
		::g_keydown = true;
		Sleep(30);
		::g_keydown = false;
	}

	//===============================================================================================
	//Arrow left  //Pressing this will rewind the scene by 1 tick
	else if ((GetKeyState(VK_LEFT) < 0) && !::g_keydown) {
		::g_keydown = true;
		Sleep(30);
		::g_keydown = false;
	}

	//A   //Changing volume/pitch/etc whatever is selected down
	else if ((GetKeyState(0x41) < 0) && !::g_keydown) {
		::g_keydown = true;
		Sleep(200);
		::g_keydown = false;
	}
	//===============================================================================================
	//Number 1
	else if ((GetKeyState(0x31) < 0) && !::g_keydown) { //Key down
		::g_keydown = true;
		//YOUR CODE HERE
		bool paused = true;
		mediaObjects[0].channel->getPaused(&paused);
		mediaObjects[0].channel->setPaused(!paused);
		Sleep(200);
		::g_keydown = false;
	}

	//===============================================================================================
	//Number 2
	else if ((GetKeyState(0x32) < 0) && !::g_keydown) {
		::g_keydown = true;
		//YOUR CODE HERE
		bool paused = true;
		mediaObjects[1].channel->getPaused(&paused);
		mediaObjects[1].channel->setPaused(!paused);
		Sleep(200);
		::g_keydown = false;
	}

	//===============================================================================================
	//Number 3
	else if ((GetKeyState(0x33) < 0) && !::g_keydown) {
		::g_keydown = true;
		//YOUR CODE HERE
		bool paused = true;
		mediaObjects[2].channel->getPaused(&paused);
		mediaObjects[2].channel->setPaused(!paused);

		Sleep(200);
		::g_keydown = false;
	}
	//===============================================================================================
	//Number 4
	else if ((GetKeyState(0x34) < 0) && !::g_keydown) {
		::g_keydown = true;
		recording = true;
		g_result = g_psystem->recordStart(DEVICE_INDEX, mediaObjects[3].sound, true);
		checkForErrors();
		//TODO: Play recording sound
		g_result = g_psystem->playSound(mediaObjects[3].sound, 0, false, &mediaObjects[3].channel);
		checkForErrors();

		mediaObjects[3].channel->setChannelGroup(group_a);
		Sleep(200);
		::g_keydown = false;
	}
	//Number 5
	else if ((GetKeyState(0x35) < 0) && !::g_keydown) {
		::g_keydown = true;
		bool bypass = true;
		dsplowpass->getBypass(&bypass);
		dsplowpass->setBypass(!bypass);
		checkForErrors();
		Sleep(200);
		::g_keydown = false;
	}
	//Number 6
	else if ((GetKeyState(0x36) < 0) && !::g_keydown) {
		::g_keydown = true;
		bool bypass = true;
		dspreverb->getBypass(&bypass);
		dspreverb->setBypass(!bypass);
		checkForErrors();
		Sleep(200);
		::g_keydown = false;
	}
	//Number 7
	else if ((GetKeyState(0x37) < 0) && !::g_keydown) {
		::g_keydown = true;
		bool bypass = true;
		dspecho->getBypass(&bypass);
		dspecho->setBypass(false);
		checkForErrors();
		Sleep(200);
		::g_keydown = false;
	}
	//Number 8
	else if ((GetKeyState(0x38) < 0) && !::g_keydown) {
		::g_keydown = true;
		bool bypass = true;
		dspdistortion->getBypass(&bypass);
		dspdistortion->setBypass(false);
		checkForErrors();
		Sleep(200);
		::g_keydown = false;
	}
	//Number 9
	else if ((GetKeyState(0x39) < 0) && !::g_keydown) {
		::g_keydown = true;
		bool bypass = true;
		dsppitchshift->getBypass(&bypass);
		dsppitchshift->setBypass(false);
		checkForErrors();
		Sleep(200);
		::g_keydown = false;
	}
	//Number 0
	else if ((GetKeyState(0x40) < 0) && !::g_keydown) {
		::g_keydown = true;
		bool bypass = true;
		dsptremolo->getBypass(&bypass);
		dsptremolo->setBypass(false);
		checkForErrors();
		Sleep(200);
		::g_keydown = false;
	}

}

int main()
{

	init_fmod();

	//load the text from file
	//load the text from the other file to save to a wav
	bool loaded = LoadSoundsFromFile();
	

	 hr2 = S_OK;



	//TODO: Initialize the COM library on the current thread
	if (FAILED(::CoInitialize(NULL))) {
		return false;
	}


	//TODO: Initialize voice.
	hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void **)&pVoice);
	//TODO: create voice
	hr2 = cpVoice.CoCreateInstance(CLSID_SpVoice);


	//LoadSoundsFromFileToWav();


	//TODO: Identify recording drivers in your pc
	int numberofdrivers = 0;

	int numberoftries = 20;
	while (numberoftries > 0 && numberofdrivers <= 0)
	{
		numberoftries--;
		Sleep(100);
		g_result = g_psystem->getRecordNumDrivers(0, &numberofdrivers);
		checkForErrors();
	}




	if (numberofdrivers == 0)
	{
		//no recording devices available. 
		exit(EXIT_FAILURE);
	}

	//get instance of master channel
	::g_result = ::g_psystem->getMasterChannelGroup(&group_master);
	checkForErrors();

	//create channel groups
	::g_result = ::g_psystem->createChannelGroup("Group A", &group_a);
	checkForErrors();

	//create channel groups
	::g_result = ::g_psystem->createChannelGroup("Group A", &group_b);
	checkForErrors();

	//Add channel groups to master
	::g_result = group_master->addGroup(group_a);
	checkForErrors();

	::g_result = group_master->addGroup(group_b);
	checkForErrors();



	//Radio station sounds
	MediaObject m;
	mediaObjects.push_back(m);
	MediaObject m1;
	mediaObjects.push_back(m1);
	MediaObject m2;
	mediaObjects.push_back(m2);

	//Recorded sound
	MediaObject m3;
	mediaObjects.push_back(m3);


	group_a->setMute(false);
	group_b->setMute(false);

	//Create DSP effects
	::g_result = ::g_psystem->createDSPByType(FMOD_DSP_TYPE_LOWPASS, &dsplowpass);
	checkForErrors();
	::g_result = ::g_psystem->createDSPByType(FMOD_DSP_TYPE_ECHO, &dspecho);
	checkForErrors();
	::g_result = ::g_psystem->createDSPByType(FMOD_DSP_TYPE_SFXREVERB, &dspreverb);
	checkForErrors();

	::g_result = ::g_psystem->createDSPByType(FMOD_DSP_TYPE_DISTORTION, &dspdistortion);
	checkForErrors();
	::g_result = ::g_psystem->createDSPByType(FMOD_DSP_TYPE_PITCHSHIFT, &dsppitchshift);
	checkForErrors();
	::g_result = ::g_psystem->createDSPByType(FMOD_DSP_TYPE_TREMOLO, &dsptremolo);

	group_a->addDSP(0, dsplowpass);
	checkForErrors();
	dsplowpass->setBypass(true);
	checkForErrors();

	group_a->addDSP(0, dspecho);
	checkForErrors();
	dspecho->setBypass(true);
	checkForErrors();

	group_a->addDSP(0, dspreverb);
	checkForErrors();
	dspreverb->setBypass(true);
	checkForErrors();



	group_b->addDSP(0, dspdistortion);
	checkForErrors();
	dspdistortion->setBypass(true);
	checkForErrors();

	group_b->addDSP(0, dsppitchshift);
	checkForErrors();
	dsppitchshift->setBypass(true);
	checkForErrors();

	group_b->addDSP(0, dsptremolo);
	checkForErrors();
	dsptremolo->setBypass(true);
	checkForErrors();



	//TODO: Get recording driver info
	int nativeRate = 0;
	int nativeChannels = 0;
	g_result = g_psystem->getRecordDriverInfo(DEVICE_INDEX, NULL, 0, NULL, &nativeRate, NULL, &nativeChannels, NULL);
	checkForErrors();

	//TODO: Create user sound
	FMOD_CREATESOUNDEXINFO exinfo = { 0 };
	exinfo.cbsize = sizeof(FMOD_CREATESOUNDEXINFO);
	exinfo.numchannels = nativeChannels;
	exinfo.format = FMOD_SOUND_FORMAT_PCM16;
	exinfo.defaultfrequency = nativeRate;
	exinfo.length = nativeRate * sizeof(short) * nativeChannels;

	g_result = g_psystem->createSound(0, FMOD_LOOP_NORMAL | FMOD_OPENUSER, &exinfo, &mediaObjects[3].sound);
	checkForErrors();


	//TODO: 
	//Increase internal buffersize for streams opened to account for Internet lag, default is 16384 
	g_result = g_psystem->setStreamBufferSize(STREAM_BUFFER_SIZE, FMOD_TIMEUNIT_RAWBYTES);
	checkForErrors();

	//TODO: 
	//Create sound using an internet url
	g_result = g_psystem->createSound(murl_80s_1, FMOD_CREATESTREAM | FMOD_NONBLOCKING, 0, &mediaObjects[0].sound);
	checkForErrors();
	g_result = g_psystem->createSound(murl_80s_2, FMOD_CREATESTREAM | FMOD_NONBLOCKING, 0, &mediaObjects[1].sound);
	checkForErrors();
	g_result = g_psystem->createSound(murl_80s_3, FMOD_CREATESTREAM | FMOD_NONBLOCKING, 0, &mediaObjects[2].sound);
	checkForErrors();


	if (loaded) {

		//the variables needed to display all the information about the currently selected sound
		//declared here so they aren't declared everytime the main loop runs
		string soundName = "";
		FMOD_SOUND_TYPE soundType = FMOD_SOUND_TYPE_UNKNOWN;
		FMOD_SOUND_FORMAT soundFormat = FMOD_SOUND_FORMAT_NONE;
		unsigned int soundLength;
		bool is_playing = false;
		bool is_paused = false;
		bool is_starving = false;
		unsigned int position = 0;
		FMOD_TAG tag;

		//Information we are acquiring but not using
		int soundChannels;
		int soundBits;

		g_psystem->playSound(mediaObjects[0].sound, 0, true, &mediaObjects[0].channel);
		g_psystem->playSound(mediaObjects[1].sound, 0, true, &mediaObjects[1].channel);
		g_psystem->playSound(mediaObjects[2].sound, 0, true, &mediaObjects[2].channel);

		while (!::g_mis_esc)
		{
			//Needed for print_text
			start_text();
			handle_keyboard();


			g_result = g_psystem->update();


			for (int i = 0; i < 3; ++i)
			{
				if (mediaObjects[i].channel) {

					//TODO:
					//Retrieve descriptive tag stored by the sound
					while (mediaObjects[i].sound->getTag(0, -1, &tag) == FMOD_OK)
					{
						if (tag.datatype == FMOD_TAGDATATYPE_STRING)
						{
							sprintf(mtag_string[mtag_index], "%s = %s (%d bytes)", tag.name, (char*)tag.data, tag.datalen);
							//increase and limit mtag_index
							mtag_index = (mtag_index + 1) % NUMBER_OF_TAGS;
						}
						else if (tag.type == FMOD_TAGTYPE_FMOD)
						{
							//when a song changes, the sample rate might also change
							float frequency = *((float*)tag.data);
							g_result = mediaObjects[i].channel->setFrequency(frequency);
							checkForErrors();
						}
					}


					//TODO: 
					//Get channel settings
					g_result = mediaObjects[i].channel->getPaused(&is_paused);
					checkForErrors();
					g_result = mediaObjects[i].channel->isPlaying(&is_playing);
					checkForErrors();
					g_result = mediaObjects[i].channel->getPosition(&position, FMOD_TIMEUNIT_MS);
					checkForErrors();
					g_result = mediaObjects[i].channel->setMute(is_starving);
					checkForErrors();

				}
				else {
					//TODO: Play sound
					//This may fail if the stream isn't ready yet, so don't check the error code
					g_psystem->playSound(mediaObjects[i].sound, 0, true, &mediaObjects[i].channel);
				}
			}

			print_text("==============================================================");
			print_text("80's music stations:");
			bool paused;
			mediaObjects[0].channel->getPaused(&paused);
			print_text("Station 1 playing: %s", paused? "false": "true ");
			mediaObjects[1].channel->getPaused(&paused);
			print_text("Station 2 playing: %s", paused ? "false" : "true ");
			mediaObjects[2].channel->getPaused(&paused);
			print_text("Station 3 playing: %s", paused ? "false" : "true ");
			print_text("Recording: %s", recording ? "true " : "false");

			

			print_text("Recording effects:");
			bool bypass = true;
			dspecho->getBypass(&bypass);
			print_text("Echo effect on: %s", bypass ? "false" : "true ");
			dspreverb->getBypass(&bypass);
			print_text("Reverb effect on: %s", bypass ? "false" : "true ");
			dsplowpass->getBypass(&bypass);
			print_text("Low pass effect on: %s", bypass ? "false" : "true ");

			print_text("PRESS down arrow to play the text from file");
			print_text("PRESS up arrow to play the text from wav");
			print_text("Wav effects:");
			dspdistortion->getBypass(&bypass);
			print_text("Echo effect on: %s", bypass ? "false" : "true ");
			dsppitchshift->getBypass(&bypass);
			print_text("Reverb effect on: %s", bypass ? "false" : "true ");
			dsptremolo->getBypass(&bypass);
			print_text("Low pass effect on: %s", bypass ? "false" : "true ");


			//print_text("Playback speed: %.1fx    ", ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->playBackSpeed);

			//Needed for print_text
			end_text();

			Sleep(50);
		}
	}
	else
	{
		cout << "Failed to load from text file, please check README.txt for wanted formatting" << endl;
	}
	pVoice->Release();
	pVoice = NULL;
	cpStream.Release();
	cpVoice.Release();
	::CoUninitialize();

	group_a->release();
	group_b->release();
	if (::g_psystem) {
		::g_result = ::g_psystem->close();
		checkForErrors();
		::g_result = ::g_psystem->release();
		checkForErrors();
	}

	return 0;
}