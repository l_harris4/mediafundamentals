#ifndef _cSoundPlayerHG_
#define _cSoundPlayerHG_
#include <vector>
#include "MediaObject.h"
#include "utils.h"


enum eChangingAttribute {
	VOLUME = 0,
	BALANCE,
	PITCH,
	PLAYBACKSPEED,
	FREQUENCY
};

class cSoundPlayer
{
public:
	cSoundPlayer();
	~cSoundPlayer();
	//methods
	void Print();
	void ChangeAttributeUp();
	void ChangeAttributeDown();
	void ModifyAttributeUp();
	void ModifyAttributeDown();
	void ChangeSelectedUp();
	void ChangeSelectedDown();
	void ChangeSelectedSection(int section);
	void DSPEffects(int id);
	void UpdateListenerPos(glm::vec3 newPos);
	void UpdateSoundPos(int id, glm::vec3 newPos);
	void UpdateSystem();
	void Startup();
	void Stop();
	
	//Master and groups
	FMOD::ChannelGroup *group_a, *group_b, *group_c, *group_d, *group_master;
	FMOD::ChannelGroup *selectedGroupPointer;
	//data members
	eChangingAttribute changingAttribute = eChangingAttribute::VOLUME;
	float masterPan;
	float aPan;
	float bPan;
	float cPan;
	float selectedGroupPan;
	int selectedGroup = 0;
	int selectedSoundIndex;
	int changingSoundMode;
	std::vector<MediaObject*> mediaObjects;
	float masterPitch;
	//DSP variables
	FMOD::DSP *dsplowpass = 0;
	FMOD::DSP *dsphighpass = 0;
	FMOD::DSP *dspecho = 0;
	FMOD::DSP *dspreverb = 0;

	FMOD::DSP *dspitlowpass = 0;
	FMOD::DSP *dspdistortion = 0;
	FMOD::DSP *dsppitchshift = 0;
	FMOD::DSP *dsptremolo = 0;
	FMOD::DSP *dsposcillator = 0;

};


#endif // !_cSoundPlayerHG_
