#include "cSoundPlayer.h"
#include <string>
#include <fstream>
#include <iostream>
using namespace std;

string g_baseSoundFilePath = "../Sounds/";
FMOD_RESULT g_result;
//global variables
FMOD::System *g_psystem = NULL;

FMOD_VECTOR mlistenerposition = { 0.0f, 0.0f, -10000.0f };
FMOD_VECTOR mforward = { 0.0f, 0.0f, 1.0f };
FMOD_VECTOR mup = { 0.0f, 1.0f, 0.0f };
FMOD_VECTOR mvel = { 0.0f, 0.0f, 0.0f };

//a method to check if an error has occured
void checkForErrors() {
	if (::g_result != FMOD_OK) {
		fprintf(stderr, "FMOD error! (%d) %s\n", ::g_result, FMOD_ErrorString(::g_result));
		exit(-1);
	}
}

//for initliazing fmod
void init_fmod() {

	// Create the main system object.
	::g_result = FMOD::System_Create(&::g_psystem);
	checkForErrors();

	//Initializes the system object, and the msound device. This has to be called at the start of the user's program
	::g_result = ::g_psystem->init(512, FMOD_INIT_NORMAL, 0);    // Initialize FMOD.
	checkForErrors();

}


cSoundPlayer::cSoundPlayer()
{
	masterPitch = 1.0f;
	masterPan = 0.0f;
	aPan = 0.0f;
	bPan = 0.0f;
	cPan = 0.0f;
	selectedGroupPan = 0.0f;
	init_fmod();
	//Here is where you will go through a text file and load all the needed sounds,
	//maybe their locations in the party as well
	string filename = "SoundsList.txt";
	string line;
	ifstream inFile(filename.c_str());
	MediaObject tempMediaObject;
	bool inScene = false;
	bool inSample = false;
	int groupIndex = -1;
	selectedSoundIndex = 0;
	changingSoundMode = 0;

	//get instance of master channel
	::g_result = ::g_psystem->getMasterChannelGroup(&group_master);
	checkForErrors();

	//create channel groups
	::g_result = ::g_psystem->createChannelGroup("Group A", &group_a);
	checkForErrors();
	selectedGroupPointer = group_a;

	::g_result = ::g_psystem->createChannelGroup("Group B", &group_b);
	checkForErrors();

	::g_result = ::g_psystem->createChannelGroup("Group C", &group_c);
	checkForErrors();

	::g_result = ::g_psystem->createChannelGroup("Group D", &group_d);
	checkForErrors();

	//Add channel groups to master
	::g_result = group_master->addGroup(group_a);
	checkForErrors();
	::g_result = group_master->addGroup(group_b);
	checkForErrors();
	::g_result = group_master->addGroup(group_c);
	checkForErrors();
	::g_result = group_master->addGroup(group_d);
	checkForErrors();




	try {
		while (getline(inFile, line))
		{
			if (line == "")
			{
				continue;
			}


			if (line.find("stream:") != string::npos) {
				//create a new media object and load that into the scene
				line.replace(0, 8, "");
				FMOD::Sound* sound1;
				FMOD::Channel* channel1 = 0;
				tempMediaObject.fileName = line;
				line = ::g_baseSoundFilePath + line;
				::g_result = ::g_psystem->createSound(line.c_str(), FMOD_CREATESTREAM, 0, &sound1);
				checkForErrors();

				::g_result = sound1->setMode(FMOD_LOOP_NORMAL);
				checkForErrors();

				::g_result = ::g_psystem->playSound(sound1, 0, false, &channel1);
				checkForErrors();

				channel1->setVolume(0.2f);
				checkForErrors();

				//assign sound and channel to the temp media object
				tempMediaObject.sound = sound1;
				tempMediaObject.channel = channel1;
				//make a new mediaobject
				MediaObject* mediaObject = new MediaObject(tempMediaObject);
				mediaObject->streamObject = true;
				mediaObject->volume = 0.2f;
				mediaObject->groupID = groupIndex;
				//put the media object on the backest scene object
				switch (groupIndex)
				{
				case 0:
					mediaObject->channel->setChannelGroup(group_a);
					break;
				case 1:
					mediaObject->channel->setChannelGroup(group_b);
					break;
				case 2:
					mediaObject->channel->setChannelGroup(group_c);
					break;
				case 3:
					mediaObject->channel->setChannelGroup(group_d);
					break;
				}

				mediaObjects.push_back(mediaObject);


				continue;
			}
			if (line.find("3D:") != string::npos) {
				//create a new media object and load that into the scene
				line.replace(0, 4, "");
				FMOD::Sound* sound1;
				FMOD::Channel* channel1 = 0;
				tempMediaObject.fileName = line;
				line = ::g_baseSoundFilePath + line;
				FMOD_VECTOR sound_position = { 0.0f, 0.0f, 0.0f };
				FMOD_VECTOR sound_velocity = { 0.0f, 0.0f, 0.0f };

				::g_result = ::g_psystem->createSound(line.c_str(), FMOD_3D, 0, &sound1);
				checkForErrors();

				::g_result = sound1->setMode(FMOD_LOOP_NORMAL);
				checkForErrors();

				::g_result = ::g_psystem->playSound(sound1, 0, false, &channel1);
				checkForErrors();

				channel1->set3DAttributes(&sound_position, &sound_velocity);
				checkForErrors();

				channel1->setVolume(0.2f);
				checkForErrors();

				//assign sound and channel to the temp media object
				tempMediaObject.sound = sound1;
				tempMediaObject.channel = channel1;
				//make a new mediaobject
				MediaObject* mediaObject = new MediaObject(tempMediaObject);
				mediaObject->streamObject = true;
				mediaObject->volume = 0.2f;
				mediaObject->groupID = groupIndex;
				//put the media object on the backest scene object
				switch (groupIndex)
				{
				case 0:
					mediaObject->channel->setChannelGroup(group_a);
					break;
				case 1:
					mediaObject->channel->setChannelGroup(group_b);
					break;
				case 2:
					mediaObject->channel->setChannelGroup(group_c);
					break;
				case 3:
					mediaObject->channel->setChannelGroup(group_d);
					break;
				}

				mediaObjects.push_back(mediaObject);


				continue;
			}
			if (line.find("Group Start") != string::npos) {
				groupIndex++;
				continue;
				//change the timesplayed for the most recent media object
			}
			if (line.find("Group End") != string::npos) {
				continue;
				//change the timesplayed for the most recent media object
			}

			if (line.find("volume: ") != string::npos) {
				line.replace(0, 8, "");
				mediaObjects.back()->volume = atof(line.c_str());
				mediaObjects.back()->channel->setVolume(mediaObjects.back()->volume);
				continue;
				//change the timesplayed for the most recent media object
			}
			if (line.find("position: ") != std::string::npos) {
				line.replace(0, 10, "");

				mediaObjects.back()->ThirdDimension = true;

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						mediaObjects.back()->position.x = stof(number);
						number = "";
					}
					else
					{
						mediaObjects.back()->position.y = stof(number);
						number = "";
					}
				}
				mediaObjects.back()->position.z = stof(number);

				FMOD_VECTOR sound_position = { mediaObjects.back()->position.x,
					0.0f,
					mediaObjects.back()->position.z };
				FMOD_VECTOR sound_velocity = { 0.0f, 0.0f, 0.0f };

				mediaObjects.back()->channel->set3DAttributes(&sound_position, &sound_velocity);
				checkForErrors();

				checkForErrors();
				continue;
			}
		}

	}
	catch (...)
	{
	}

	//set 3d listener attributes
	::g_result = ::g_psystem->set3DListenerAttributes(0, &mlistenerposition, &mvel, &mforward, &mup);
	checkForErrors();

	//group_master->setMute(true);
	group_a->setMute(false);
	group_b->setMute(true);
	group_c->setMute(true);
	group_d->setMute(false);

	//Create DSP effects
	::g_result = ::g_psystem->createDSPByType(FMOD_DSP_TYPE_LOWPASS, &dsplowpass);
	checkForErrors();
	::g_result = ::g_psystem->createDSPByType(FMOD_DSP_TYPE_HIGHPASS, &dsphighpass);
	checkForErrors();
	::g_result = ::g_psystem->createDSPByType(FMOD_DSP_TYPE_ECHO, &dspecho);
	checkForErrors();

	::g_result = ::g_psystem->createDSPByType(FMOD_DSP_TYPE_SFXREVERB, &dspreverb);
	checkForErrors();
	::g_result = ::g_psystem->createDSPByType(FMOD_DSP_TYPE_ITLOWPASS, &dspitlowpass);
	checkForErrors();
	::g_result = ::g_psystem->createDSPByType(FMOD_DSP_TYPE_DISTORTION, &dspdistortion);
	checkForErrors();


	::g_result = ::g_psystem->createDSPByType(FMOD_DSP_TYPE_PITCHSHIFT, &dsppitchshift);
	checkForErrors();
	::g_result = ::g_psystem->createDSPByType(FMOD_DSP_TYPE_TREMOLO, &dsptremolo);
	checkForErrors();
	::g_result = ::g_psystem->createDSPByType(FMOD_DSP_TYPE_OSCILLATOR, &dsposcillator);
	checkForErrors();


	group_a->addDSP(0, dsplowpass);
	checkForErrors();
	dsplowpass->setBypass(true);
	checkForErrors();

	group_a->addDSP(0, dsphighpass);
	checkForErrors();
	dsphighpass->setBypass(true);
	checkForErrors();

	group_a->addDSP(0, dspecho);
	checkForErrors();
	dspecho->setBypass(true);
	checkForErrors();


	group_b->addDSP(0, dspreverb);
	checkForErrors();
	dspreverb->setBypass(true);
	checkForErrors();

	group_b->addDSP(0, dspitlowpass);
	checkForErrors();
	dspitlowpass->setBypass(true);
	checkForErrors();

	group_b->addDSP(0, dspdistortion);
	checkForErrors();
	dspdistortion->setBypass(true);
	checkForErrors();


	group_c->addDSP(0, dsppitchshift);
	checkForErrors();
	dsppitchshift->setBypass(true);
	checkForErrors();

	group_c->addDSP(0, dsptremolo);
	checkForErrors();
	dsptremolo->setBypass(true);
	checkForErrors();

	group_c->addDSP(0, dsposcillator);
	checkForErrors();
	dsposcillator->setBypass(true);
	checkForErrors();



}

cSoundPlayer::~cSoundPlayer()
{
	int size = mediaObjects.size();
	for (int i = 0; i < mediaObjects.size(); i++)
	{
		mediaObjects[i]->Release();
		delete mediaObjects[i];
	}
	group_a->release();
	group_b->release();
	group_c->release();
	group_d->release();

}

void cSoundPlayer::Print()
{
	start_text();
	print_text("==============================================================");
	print_text("Get your friends out of the party                       ");
	if (changingSoundMode == 0)
		print_text("Changing : single sound                          ");
	else if (changingSoundMode == 1)
		print_text("Changing : all sounds                           ");
	else
		print_text("Changing : sounds in group of selected sound           ");

	print_text("Selected File: %s   ", mediaObjects[selectedSoundIndex]->fileName.c_str());
	print_text("Sound is in group: %i   ", mediaObjects[selectedSoundIndex]->groupID);
	print_text("Current Group Playing: %i", selectedGroup);


	if (changingSoundMode != 2) {
		float mVolume;
		group_master->getVolume(&mVolume);
		print_text("Master group volume: %.2f", mVolume);
		print_text("Master group balance: %.2f", masterPan);
		print_text("Master group pitch: %f", masterPitch);
	}
	else {
		float mVolume;
		selectedGroupPointer->getVolume(&mVolume);
		print_text("Group volume: %.2f            ", mVolume);
		print_text("Group balance: %.2f           ", selectedGroupPan);
		float mPitch;
		selectedGroupPointer->getPitch(&mPitch);
		print_text("Group pitch: %.1f              ", mPitch);
	}

	switch (changingAttribute)
	{
	case eChangingAttribute::VOLUME:
		print_text("Use up and down to change which attribute of the sound is being changed. Currently changing: VOLUME     ");
		break;
	case eChangingAttribute::BALANCE:
		print_text("Use up and down to change which attribute of the sound is being changed. Currently changing: BALANCE      ");
		break;
	case eChangingAttribute::PITCH:
		print_text("Use up and down to change which attribute of the sound is being changed. Currently changing: PITCH         ");
		break;
	case eChangingAttribute::PLAYBACKSPEED:
		print_text("Use up and down to change which attribute of the sound is being changed. Currently changing: PLAYBACKSPEED    ");
		break;
	case eChangingAttribute::FREQUENCY:
		print_text("Use up and down to change which attribute of the sound is being changed. Currently changing: FREQUENCY    ");
		break;
	}
	print_text("Volume: %.2f    ", mediaObjects[selectedSoundIndex]->volume);
	print_text("Balance: %.1f     ", mediaObjects[selectedSoundIndex]->pan);
	print_text("Playback speed: %.1fx    ", mediaObjects[selectedSoundIndex]->playBackSpeed);
	print_text("Pitch: %.2f    ", mediaObjects[selectedSoundIndex]->pitch);
	print_text("Frequency: %.2f Hz   ", mediaObjects[selectedSoundIndex]->frequency);
	//Get dsp status
	bool dsplowpass_bypass;
	bool dsphighpass_bypass;
	bool dspecho_bypass;
	bool dspreverb_bypass;
	bool dspitlowpass_bypass;
	bool dspdistortion_bypass;
	bool dsppitchshift_bypass;
	bool dsptremolo_bypass;
	bool dsposillator_bypass;
	dsplowpass->getBypass(&dsplowpass_bypass);
	dsphighpass->getBypass(&dsphighpass_bypass);
	dspecho->getBypass(&dspecho_bypass);
	dspreverb->getBypass(&dspreverb_bypass);
	dspitlowpass->getBypass(&dspitlowpass_bypass);
	dspdistortion->getBypass(&dspdistortion_bypass);
	dsppitchshift->getBypass(&dsppitchshift_bypass);
	dsptremolo->getBypass(&dsptremolo_bypass);
	dsposcillator->getBypass(&dsposillator_bypass);

	print_text("Effect status: lowpass[%c] highpass[%c] echo[%c]",
		dsplowpass_bypass ? ' ' : 'x',
		dsphighpass_bypass ? ' ' : 'x',
		dspecho_bypass ? ' ' : 'x');

	print_text("Effect status: reverb[%c] itlowpass[%c] distortion[%c]",
		dspreverb_bypass ? ' ' : 'x',
		dspitlowpass_bypass ? ' ' : 'x',
		dspdistortion_bypass ? ' ' : 'x');

	print_text("Effect status: pitchshift[%c] tremolo[%c] oscillator[%c]",
		dsppitchshift_bypass ? ' ' : 'x',
		dsptremolo_bypass ? ' ' : 'x',
		dsposillator_bypass ? ' ' : 'x');


	end_text();
}

void cSoundPlayer::ChangeAttributeUp()
{
	switch (changingAttribute)
	{
	case eChangingAttribute::VOLUME:
		changingAttribute = eChangingAttribute::BALANCE;
		break;
	case eChangingAttribute::BALANCE:
		changingAttribute = eChangingAttribute::PITCH;
		break;
	case eChangingAttribute::PITCH:
		changingAttribute = eChangingAttribute::PLAYBACKSPEED;
		break;
	case eChangingAttribute::PLAYBACKSPEED:
		changingAttribute = eChangingAttribute::FREQUENCY;
		break;
	case eChangingAttribute::FREQUENCY:
		changingAttribute = eChangingAttribute::VOLUME;
		break;
	}
}

void cSoundPlayer::ChangeAttributeDown()
{
	switch (changingAttribute)
	{
	case eChangingAttribute::VOLUME:
		changingAttribute = eChangingAttribute::FREQUENCY;
		break;
	case eChangingAttribute::BALANCE:
		changingAttribute = eChangingAttribute::VOLUME;
		break;
	case eChangingAttribute::PITCH:
		changingAttribute = eChangingAttribute::BALANCE;
		break;
	case eChangingAttribute::PLAYBACKSPEED:
		changingAttribute = eChangingAttribute::PITCH;
		break;
	case eChangingAttribute::FREQUENCY:
		changingAttribute = eChangingAttribute::PLAYBACKSPEED;
		break;
	}
}

void cSoundPlayer::ModifyAttributeUp()
{
	if (changingSoundMode == 1) {
		switch (changingAttribute)
		{
			//changing the volume up
		case eChangingAttribute::VOLUME:
			float volume;
			group_master->getVolume(&volume);
			if (volume < 1)
			{
				group_master->setVolume(volume + 0.1);
			}
			break;
			//changing the balance right
		case eChangingAttribute::BALANCE:
			if (masterPan < 1)
			{
				group_master->setPan(masterPan + 0.1);
				masterPan += 0.1;
			}
			break;
			//increasing the pitch
		case eChangingAttribute::PITCH:

			//
			float pitch;
			group_a->getPitch(&pitch);
			if (pitch < 2.9)
			{
				group_a->setPitch(pitch + 0.1f);
			}

			float pitch1;
			group_b->getPitch(&pitch1);
			if (pitch1 < 2.9)
			{
				group_b->setPitch(pitch1 + 0.1f);
			}

			float pitch2;
			group_c->getPitch(&pitch2);
			if (pitch2 < 2.9)
			{
				group_c->setPitch(pitch2 + 0.1f);
			}

			float pitch3;
			group_d->getPitch(&pitch3);
			if (pitch3 < 2.9)
			{
				group_d->setPitch(pitch3 + 0.1f);
			}

			if (masterPitch < 2.9)
			{
				masterPitch += 0.1f;
			}
			break;
			//increasing the playback speed
		case eChangingAttribute::PLAYBACKSPEED:
			for (int soundIndex = 0; soundIndex < mediaObjects.size(); soundIndex++) {
				if (mediaObjects[soundIndex]->channel) {
					float tempPlayBackSpeed = mediaObjects[soundIndex]->playBackSpeed;
					if (tempPlayBackSpeed == 0.5) {
						::g_result = mediaObjects[soundIndex]->channel->setFrequency(44100);
						mediaObjects[soundIndex]->playBackSpeed = 1.0f;
						mediaObjects[soundIndex]->frequency = 44100;
					}
					else if (tempPlayBackSpeed == 1.0) {
						::g_result = mediaObjects[soundIndex]->channel->setFrequency(44100 * 2);
						mediaObjects[soundIndex]->playBackSpeed = 2.0;
						mediaObjects[soundIndex]->frequency = 44100 * 2;
					}
				}
			}
			break;
			//increasing the frequency
		case eChangingAttribute::FREQUENCY:
			for (int soundIndex = 0; soundIndex < mediaObjects.size(); soundIndex++) {
				if (mediaObjects[soundIndex]->channel) {
					float tempFrequency = mediaObjects[soundIndex]->frequency;
					if (tempFrequency < 80000) {
						::g_result = mediaObjects[soundIndex]->channel->setFrequency(tempFrequency + 100);
						mediaObjects[soundIndex]->frequency += 100;
					}
				}
			}
			break;
		}
	}
	else if (changingSoundMode == 0) //only changing one sound
	{
		switch (changingAttribute)
		{
			//increasing the volume
		case eChangingAttribute::VOLUME:
			if (mediaObjects[selectedSoundIndex]->channel) {
				float tempVolume = mediaObjects[selectedSoundIndex]->volume;
				if (tempVolume < 1) {
					::g_result = mediaObjects[selectedSoundIndex]->channel->setVolume(tempVolume + 0.1);
					mediaObjects[selectedSoundIndex]->volume += 0.1;
				}
			}
			break;
			//changing the balance right
		case eChangingAttribute::BALANCE:
			if (mediaObjects[selectedSoundIndex]->channel) {
				if (mediaObjects[selectedSoundIndex]->pan < 1) {
					::g_result = mediaObjects[selectedSoundIndex]->channel->setPan(mediaObjects[selectedSoundIndex]->pan + 0.1);
					mediaObjects[selectedSoundIndex]->pan += 0.1;
				}
			}
			break;
			//increasing the pitch
		case eChangingAttribute::PITCH:
			if (mediaObjects[selectedSoundIndex]->channel) {
				float tempPitch = mediaObjects[selectedSoundIndex]->pitch;
				if (tempPitch < 3) {
					::g_result = mediaObjects[selectedSoundIndex]->channel->setPitch(tempPitch + 0.1);
					mediaObjects[selectedSoundIndex]->pitch += 0.1;
				}
			}
			break;
			//increasing the playback speed
		case eChangingAttribute::PLAYBACKSPEED:
		{
			float tempPlayBackSpeed = mediaObjects[selectedSoundIndex]->playBackSpeed;
			if (tempPlayBackSpeed == 0.5) {
				::g_result = mediaObjects[selectedSoundIndex]->channel->setFrequency(44100);
				mediaObjects[selectedSoundIndex]->playBackSpeed = 1.0f;
				mediaObjects[selectedSoundIndex]->frequency = 44100;
			}
			if (tempPlayBackSpeed == 1.0) {
				::g_result = mediaObjects[selectedSoundIndex]->channel->setFrequency(44100 * 2);
				mediaObjects[selectedSoundIndex]->playBackSpeed = 2.0;
				mediaObjects[selectedSoundIndex]->frequency = 44100 * 2;
			}
		}
		break;
		//increasing the frequency
		case eChangingAttribute::FREQUENCY:
		{
			float tempFrequency = mediaObjects[selectedSoundIndex]->frequency;
			if (tempFrequency < 80000) {
				::g_result = mediaObjects[selectedSoundIndex]->channel->setFrequency(tempFrequency + 100);
				mediaObjects[selectedSoundIndex]->frequency += 100;
			}
		}
		break;
		}
	}
	else if (changingSoundMode == 2) {
		switch (changingAttribute)
		{
			//changing the volume up
		case eChangingAttribute::VOLUME:
			float volume;
			selectedGroupPointer->getVolume(&volume);
			if (volume < 1)
			{
				selectedGroupPointer->setVolume(volume + 0.1);
			}
			break;
			//changing the balance right
		case eChangingAttribute::BALANCE:
			if (selectedGroup == 0)
			{
				if (aPan < 1)
				{
					selectedGroupPointer->setPan(aPan + 0.1);
					aPan += 0.1;
				}
			}
			else if (selectedGroup == 1)
			{
				if (bPan < 1)
				{
					selectedGroupPointer->setPan(bPan + 0.1);
					bPan += 0.1;
				}
			}
			else if (selectedGroup == 2)
			{
				if (cPan < 1)
				{
					selectedGroupPointer->setPan(cPan + 0.1);
					cPan += 0.1;
				}
			}
			break;
			//increasing the pitch
		case eChangingAttribute::PITCH:
			float pitch;
			selectedGroupPointer->getPitch(&pitch);
			if (pitch < 2.9)
			{
				selectedGroupPointer->setPitch(pitch + 0.1f);
			}
			break;
			//increasing the playback speed
		case eChangingAttribute::PLAYBACKSPEED:
			for (int soundIndex = 0; soundIndex < mediaObjects.size(); soundIndex++) {
				if (mediaObjects[soundIndex]->groupID == selectedGroup) {
					if (mediaObjects[soundIndex]->channel) {
						float tempPlayBackSpeed = mediaObjects[soundIndex]->playBackSpeed;
						if (tempPlayBackSpeed == 0.5) {
							::g_result = mediaObjects[soundIndex]->channel->setFrequency(44100);
							mediaObjects[soundIndex]->playBackSpeed = 1.0f;
							mediaObjects[soundIndex]->frequency = 44100;
						}
						else if (tempPlayBackSpeed == 1.0) {
							::g_result = mediaObjects[soundIndex]->channel->setFrequency(44100 * 2);
							mediaObjects[soundIndex]->playBackSpeed = 2.0;
							mediaObjects[soundIndex]->frequency = 44100 * 2;
						}
					}
				}
			}
			break;
			//increasing the frequency
		case eChangingAttribute::FREQUENCY:
			for (int soundIndex = 0; soundIndex < mediaObjects.size(); soundIndex++) {
				if (mediaObjects[soundIndex]->groupID == selectedGroup) {
					if (mediaObjects[soundIndex]->channel) {
						float tempFrequency = mediaObjects[soundIndex]->frequency;
						if (tempFrequency < 80000) {
							::g_result = mediaObjects[soundIndex]->channel->setFrequency(tempFrequency + 100);
							mediaObjects[soundIndex]->frequency += 100;
						}
					}
				}
			}
			break;
		}
	}
}

void cSoundPlayer::ModifyAttributeDown()
{
	//if we are changing all the sounds opposed to just one
	if (changingSoundMode == 1) {
		switch (changingAttribute)
		{
			//changing volume down
		case eChangingAttribute::VOLUME:
			float volume;
			group_master->getVolume(&volume);
			if (volume > 0.05)
			{
				group_master->setVolume(volume - 0.1);
			}
			break;
			//changing balance left
		case eChangingAttribute::BALANCE:
			if (masterPan > -1)
			{
				group_master->setPan(masterPan - 0.1);
				masterPan -= 0.1;
			}
			break;
			//changing pitch down
		case eChangingAttribute::PITCH:
			float pitch;
			group_a->getPitch(&pitch);
			if (pitch > 0)
			{
				group_a->setPitch(pitch - 0.1f);
			}

			float pitch1;
			group_b->getPitch(&pitch1);
			if (pitch1 > 0)
			{
				group_b->setPitch(pitch1 - 0.1f);
			}

			float pitch2;
			group_c->getPitch(&pitch2);
			if (pitch2 > 0)
			{
				group_c->setPitch(pitch2 - 0.1f);
			}

			float pitch3;
			group_d->getPitch(&pitch3);
			if (pitch3 > 0)
			{
				group_d->setPitch(pitch3 - 0.1f);
			}

			if (masterPitch > 0)
			{
				masterPitch -= 0.1f;
			}
			break;
			//reducing playback speed
		case eChangingAttribute::PLAYBACKSPEED:
			for (int soundIndex = 0; soundIndex < mediaObjects.size(); soundIndex++) {
				if (mediaObjects[soundIndex]->channel) {
					float tempPlayBackSpeed = mediaObjects[soundIndex]->playBackSpeed;
					if (tempPlayBackSpeed == 1.0) {
						::g_result = mediaObjects[soundIndex]->channel->setFrequency(44100 * 0.5);
						mediaObjects[soundIndex]->playBackSpeed = 0.5;
						mediaObjects[soundIndex]->frequency = 44100 * 0.5;
					}
					if (tempPlayBackSpeed == 2.0) {
						::g_result = mediaObjects[soundIndex]->channel->setFrequency(44100);
						mediaObjects[soundIndex]->playBackSpeed = 1.0;
						mediaObjects[soundIndex]->frequency = 44100;
					}
				}
			}
			break;
			//reducing frequency
		case eChangingAttribute::FREQUENCY:
			for (int soundIndex = 0; soundIndex < mediaObjects.size(); soundIndex++) {
				if (mediaObjects[soundIndex]->channel) {
					float tempFrequency = mediaObjects[soundIndex]->frequency;
					if (tempFrequency > -80000) {
						::g_result = mediaObjects[soundIndex]->channel->setFrequency(tempFrequency - 100);
						mediaObjects[soundIndex]->frequency -= 100;
					}
				}
			}
			break;
		}
	}
	else if (changingSoundMode == 0)
	{
		switch (changingAttribute)
		{
			//changing volume down
		case eChangingAttribute::VOLUME:
			if (mediaObjects[selectedSoundIndex]->channel) {
				float tempVolume = mediaObjects[selectedSoundIndex]->volume;
				if (tempVolume > 0.05) {
					::g_result = mediaObjects[selectedSoundIndex]->channel->setVolume(tempVolume - 0.1);
					mediaObjects[selectedSoundIndex]->volume -= 0.1;
				}
			}
			break;
			//changing balance left
		case eChangingAttribute::BALANCE:
			if (mediaObjects[selectedSoundIndex]->channel) {
				if (mediaObjects[selectedSoundIndex]->pan > -1) {
					::g_result = mediaObjects[selectedSoundIndex]->channel->setPan(mediaObjects[selectedSoundIndex]->pan - 0.1);
					mediaObjects[selectedSoundIndex]->pan -= 0.1;
				}
			}
			break;
			//changing pitch down
		case eChangingAttribute::PITCH:
			if (mediaObjects[selectedSoundIndex]->channel) {
				float tempPitch = mediaObjects[selectedSoundIndex]->pitch;
				if (tempPitch > 0) {
					::g_result = mediaObjects[selectedSoundIndex]->channel->setPitch(tempPitch - 0.1);
					mediaObjects[selectedSoundIndex]->pitch -= 0.1;
				}
			}
			break;
			//reducing playback speed
		case eChangingAttribute::PLAYBACKSPEED:
		{
			float tempPlayBackSpeed = mediaObjects[selectedSoundIndex]->playBackSpeed;
			if (tempPlayBackSpeed == 1.0) {
				::g_result = mediaObjects[selectedSoundIndex]->channel->setFrequency(44100 * 0.5);
				mediaObjects[selectedSoundIndex]->playBackSpeed = 0.5;
				mediaObjects[selectedSoundIndex]->frequency = 44100 * 0.5;
			}
			if (tempPlayBackSpeed == 2.0) {
				::g_result = mediaObjects[selectedSoundIndex]->channel->setFrequency(44100);
				mediaObjects[selectedSoundIndex]->playBackSpeed = 1.0;
				mediaObjects[selectedSoundIndex]->frequency = 44100;
			}
		}
		break;
		//reducing frequency
		case eChangingAttribute::FREQUENCY:
			float tempFrequency = mediaObjects[selectedSoundIndex]->frequency;
			if (tempFrequency > -80000) {
				::g_result = mediaObjects[selectedSoundIndex]->channel->setFrequency(tempFrequency - 100);
				mediaObjects[selectedSoundIndex]->frequency -= 100;
			}
			break;
		}
	}
	else if (changingSoundMode == 2) {

		switch (changingAttribute)
		{
			//changing volume down
		case eChangingAttribute::VOLUME:
			float volume;
			selectedGroupPointer->getVolume(&volume);
			if (volume > 0.05)
			{
				selectedGroupPointer->setVolume(volume - 0.1);
			}
			break;
			//changing balance left
		case eChangingAttribute::BALANCE:
			if (selectedGroup == 0)
			{
				if (aPan > -1)
				{
					selectedGroupPointer->setPan(aPan - 0.1);
					aPan -= 0.1;
				}
			}
			else if (selectedGroup == 1)
			{
				if (bPan > -1)
				{
					selectedGroupPointer->setPan(bPan - 0.1);
					bPan -= 0.1;
				}
			}
			else if (selectedGroup == 2)
			{
				if (cPan > -1)
				{
					selectedGroupPointer->setPan(cPan - 0.1);
					cPan -= 0.1;
				}
			}

			break;
			//changing pitch down
		case eChangingAttribute::PITCH:
			float pitch;
			selectedGroupPointer->getPitch(&pitch);
			if (pitch > 0)
			{
				selectedGroupPointer->setPitch(pitch - 0.1f);
			}
			break;
			//reducing playback speed
		case eChangingAttribute::PLAYBACKSPEED:
			for (int soundIndex = 0; soundIndex < mediaObjects.size(); soundIndex++) {
				if (mediaObjects[soundIndex]->groupID == selectedGroup) {
					if (mediaObjects[soundIndex]->channel) {
						float tempPlayBackSpeed = mediaObjects[soundIndex]->playBackSpeed;
						if (tempPlayBackSpeed == 1.0) {
							::g_result = mediaObjects[soundIndex]->channel->setFrequency(44100 * 0.5);
							mediaObjects[soundIndex]->playBackSpeed = 0.5;
							mediaObjects[soundIndex]->frequency = 44100 * 0.5;
						}
						if (tempPlayBackSpeed == 2.0) {
							::g_result = mediaObjects[soundIndex]->channel->setFrequency(44100);
							mediaObjects[soundIndex]->playBackSpeed = 1.0;
							mediaObjects[soundIndex]->frequency = 44100;
						}
					}
				}
			}
			break;
			//reducing frequency
		case eChangingAttribute::FREQUENCY:
			for (int soundIndex = 0; soundIndex < mediaObjects.size(); soundIndex++) {
				if (mediaObjects[soundIndex]->groupID == selectedGroup) {
					if (mediaObjects[soundIndex]->channel) {
						float tempFrequency = mediaObjects[soundIndex]->frequency;
						if (tempFrequency > -80000) {
							::g_result = mediaObjects[soundIndex]->channel->setFrequency(tempFrequency - 100);
							mediaObjects[soundIndex]->frequency -= 100;
						}
					}
				}
			}
			break;
		}
	}
}

void cSoundPlayer::ChangeSelectedUp()
{
	selectedSoundIndex++;
	if (selectedSoundIndex >= mediaObjects.size())
	{
		selectedSoundIndex = 0;
	}
}

void cSoundPlayer::ChangeSelectedDown()
{
	selectedSoundIndex--;
	if (selectedSoundIndex < 0)
	{
		selectedSoundIndex = mediaObjects.size() - 1;
	}
}

void cSoundPlayer::ChangeSelectedSection(int section)
{
	//mute other groups
	//unmute the now chosen group
	switch (section)
	{
	case 0:
		selectedGroup = 0;
		group_a->setMute(false);
		group_b->setMute(true);
		group_c->setMute(true);
		selectedGroupPointer = group_a;
		selectedGroupPan = aPan;

		break;
	case 1:
		selectedGroup = 1;
		group_a->setMute(true);
		group_b->setMute(false);
		group_c->setMute(true);
		selectedGroupPointer = group_b;
		selectedGroupPan = bPan;
		break;
	case 2:
		selectedGroup = 2;
		group_a->setMute(true);
		group_b->setMute(true);
		group_c->setMute(false);
		selectedGroupPointer = group_c;
		selectedGroupPan = cPan;
		break;
	}
}

void cSoundPlayer::DSPEffects(int id)
{
	bool bypass;
	switch (id)
	{
	case 1:
		::g_result = dsplowpass->getBypass(&bypass);
		checkForErrors();
		::g_result = dsplowpass->setBypass(!bypass);
		checkForErrors();
		break;
	case 2:
		::g_result = dsphighpass->getBypass(&bypass);
		checkForErrors();
		::g_result = dsphighpass->setBypass(!bypass);
		checkForErrors();
		break;
	case 3:
		::g_result = dspecho->getBypass(&bypass);
		checkForErrors();
		::g_result = dspecho->setBypass(!bypass);
		checkForErrors();
		break;
	case 4:
		::g_result = dspreverb->getBypass(&bypass);
		checkForErrors();
		::g_result = dspreverb->setBypass(!bypass);
		checkForErrors();
		break;
	case 5:
		::g_result = dspitlowpass->getBypass(&bypass);
		checkForErrors();
		::g_result = dspitlowpass->setBypass(!bypass);
		checkForErrors();
		break;
	case 6:
		::g_result = dspdistortion->getBypass(&bypass);
		checkForErrors();
		::g_result = dspdistortion->setBypass(!bypass);
		checkForErrors();
		break;
	case 7:
		::g_result = dsppitchshift->getBypass(&bypass);
		checkForErrors();
		::g_result = dsppitchshift->setBypass(!bypass);
		checkForErrors();
		break;
	case 8:
		::g_result = dsptremolo->getBypass(&bypass);
		checkForErrors();
		::g_result = dsptremolo->setBypass(!bypass);
		checkForErrors();
		break;
	case 9:
		::g_result = dsposcillator->getBypass(&bypass);
		checkForErrors();
		::g_result = dsposcillator->setBypass(!bypass);
		checkForErrors();
		break;
	}
}

void cSoundPlayer::UpdateListenerPos(glm::vec3 newPos)
{
	//set 3d listener attributes
	mlistenerposition.x = newPos.x;
	mlistenerposition.z = newPos.z;
	::g_result = ::g_psystem->set3DListenerAttributes(0, &mlistenerposition, &mvel, &mforward, &mup);
	checkForErrors();
}

void cSoundPlayer::UpdateSoundPos(int id, glm::vec3 newPos)
{
	FMOD_VECTOR sound_position = { newPos.x, 0.0f, newPos.z };
	FMOD_VECTOR sound_velocity = { 0.0f, 0.0f, 0.0f };

	int counter = 0;
	for (int i = 0; i < mediaObjects.size(); i++)
	{
		if (mediaObjects[i]->ThirdDimension)
		{
			counter++;
		}
		if (counter == id)
		{
			mediaObjects[i]->channel->set3DAttributes(&sound_position, &sound_velocity);
			checkForErrors();
			break;
		}
	}

}

void cSoundPlayer::UpdateSystem()
{
	Print();
	::g_psystem->update();
}

void cSoundPlayer::Startup()
{
}

void cSoundPlayer::Stop()
{
}
