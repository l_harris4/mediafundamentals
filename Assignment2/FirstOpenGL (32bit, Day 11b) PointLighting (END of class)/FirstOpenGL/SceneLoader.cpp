// This file is used to laod the models
#include "cGameObject.h"
#include <vector>
#include "Utilities.h"		// getRandInRange()
#include <glm/glm.hpp>
#include "cLightManager.h"
#include <fstream>
#include <exception>

extern std::vector< cGameObject* >  g_vecGameObjects;
extern cLightManager*		g_pLightManager;

void SaveDataToFile()
{
	std::string filename = "ObjectsConfigOutput.txt";
	std::ofstream outFile;
	outFile.open(filename);


	int sizeObjects = ::g_vecGameObjects.size();
	for (int i = 0; i < sizeObjects; ++i)
	{
		outFile << "\nObject Start\n";
		outFile << "normals: true\n";
		outFile << "physics: ";
		::g_vecGameObjects[i]->bIsUpdatedInPhysics ? outFile << "true\n" : outFile << "false\n";
		outFile << "wireframe: ";
		::g_vecGameObjects[i]->bIsWireframe ? outFile << "true\n" : outFile << "false\n";
		outFile << "filename: " << ::g_vecGameObjects[i]->meshName << "\n";
		outFile << "position: " << ::g_vecGameObjects[i]->position.x << ","
			<< ::g_vecGameObjects[i]->position.y << ","
			<< ::g_vecGameObjects[i]->position.z << "\n";
		outFile << "orientation: " << glm::degrees(::g_vecGameObjects[i]->orientation2.x) << ","
			<< glm::degrees(::g_vecGameObjects[i]->orientation2.y) << ","
			<< glm::degrees(::g_vecGameObjects[i]->orientation2.z) << "\n";
		outFile << "scale: " << ::g_vecGameObjects[i]->scale << "\n";
		outFile << "colour: " << ::g_vecGameObjects[i]->diffuseColour.x << ","
			<< ::g_vecGameObjects[i]->diffuseColour.y << ","
			<< ::g_vecGameObjects[i]->diffuseColour.z << "\n";
		outFile << "Object End\n";
	}
	int sizeLights = ::g_pLightManager->vecLights.size();
	for (int i = 0; i < sizeLights; ++i)
	{
		outFile << "\nLight Start\n";

		outFile << "colour_l: " << ::g_pLightManager->vecLights[i].diffuse.x << ","
			<< ::g_pLightManager->vecLights[i].diffuse.y << ","
			<< ::g_pLightManager->vecLights[i].diffuse.z << "\n";

		outFile << "position_l: " << ::g_pLightManager->vecLights[i].position.x << ","
			<< ::g_pLightManager->vecLights[i].position.y << ","
			<< ::g_pLightManager->vecLights[i].position.z << "\n";

		outFile << "attentuation: " << ::g_pLightManager->vecLights[i].attenuation.x << ","
			<< ::g_pLightManager->vecLights[i].attenuation.y << ","
			<< ::g_pLightManager->vecLights[i].attenuation.z << "\n";

		if (::g_pLightManager->vecLights[i].gameObjectIndex != -1)
		{
			outFile << "normals: true\n";
			outFile << "filename_l: " << ::g_vecGameObjects[::g_pLightManager->vecLights[i].gameObjectIndex]->meshName << "\n";
		}
		outFile << "Light End\n";
	}

}

bool LoadModelsLightsFromFile()
{
	std::string filename = "ObjectsConfig.txt";
	std::string line;
	std::ifstream inFile(filename.c_str());
	int lightIndex = -1;

	try {
		while (getline(inFile, line))
		{
			if (line == "")
			{
				continue;
			}
			if (line == "Object Start") {
				cGameObject* pTempGO = new cGameObject();
				pTempGO->bIsUpdatedInPhysics = false;
				pTempGO->typeOfObject = eTypeOfObject::SPHERE;
				pTempGO->radius = 1.0f;
				::g_vecGameObjects.push_back(pTempGO);
				continue;
			}
			if (line == "Light Start") {
				::g_pLightManager->CreateLights(1, true);
				lightIndex++;
				continue;
			}

			if (line.find("filename:") != std::string::npos) {
				line.replace(0, 10, "");
				::g_vecGameObjects.back()->meshName = line;
				continue;
			}
			if (line.find("physics:") != std::string::npos) {
				line.replace(0, 9, "");
				::g_vecGameObjects.back()->bIsUpdatedInPhysics = line == "true";
				continue;
			}
			if (line.find("wireframe:") != std::string::npos) {
				line.replace(0, 11, "");
				::g_vecGameObjects.back()->bIsWireframe = line == "true";
				continue;
			}
			if (line.find("position: ") != std::string::npos) {
				line.replace(0, 10, "");
				
				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_vecGameObjects.back()->position.x = stof(number);
						number = "";
					}
					else
					{
						::g_vecGameObjects.back()->position.y = stof(number);
						number = "";
					}
				}
				::g_vecGameObjects.back()->position.z = stof(number);
				continue;
			}
			if (line.find("orientation: ") != std::string::npos) {
				line.replace(0, 13, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_vecGameObjects.back()->orientation2.x = glm::radians(stof(number));
						number = "";
					}
					else
					{
						::g_vecGameObjects.back()->orientation2.y = glm::radians(stof(number));
						number = "";
					}
				}
				::g_vecGameObjects.back()->orientation2.z = glm::radians(stof(number));
				continue;
			}
			if (line.find("scale:") != std::string::npos) {
				line.replace(0, 7, "");
				::g_vecGameObjects.back()->scale = stof(line);
				continue;
			}
			if (line.find("colour:") != std::string::npos) {
				line.replace(0, 8, "");

				std::string number;
				bool rValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (rValue)
					{
						rValue = false;
						::g_vecGameObjects.back()->diffuseColour.x = stof(number);
						number = "";
					}
					else
					{
						::g_vecGameObjects.back()->diffuseColour.y = stof(number);
						number = "";
					}
				}
				::g_vecGameObjects.back()->diffuseColour.z = stof(number);
				::g_vecGameObjects.back()->diffuseColour.a = 1.0f;
				continue;
				//change the times for the most recent media object
			}
			if (line.find("filename_l:") != std::string::npos) {
				line.replace(0, 12, "");
				::g_pLightManager->vecLights[lightIndex].fileName = line;
				cGameObject* pTempGO = new cGameObject();
				pTempGO->position = ::g_pLightManager->vecLights[lightIndex].position;
				::g_pLightManager->vecLights[lightIndex].gameObjectIndex = ::g_vecGameObjects.size();
				pTempGO->diffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
				pTempGO->meshName = line;
				pTempGO->typeOfObject = eTypeOfObject::SPHERE;
				pTempGO->bIsUpdatedInPhysics = false;
				pTempGO->scale = 1;
				pTempGO->radius = 1.0f;
				::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
				continue;
			}
			if (line.find("attentuation:") != std::string::npos) {
				line.replace(0, 14, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_pLightManager->vecLights[lightIndex].attenuation.x = stof(number);
						number = "";
					}
					else
					{
						::g_pLightManager->vecLights[lightIndex].attenuation.y = stof(number);
						number = "";
					}
				}
				::g_pLightManager->vecLights[lightIndex].attenuation.z = stof(number);
				continue;
			}
			if (line.find("position_l:") != std::string::npos) {
				line.replace(0, 12, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_pLightManager->vecLights[lightIndex].position.x = stof(number);
						number = "";
					}
					else
					{
						::g_pLightManager->vecLights[lightIndex].position.y = stof(number);
						number = "";
					}
				}
				::g_pLightManager->vecLights[lightIndex].position.z = stof(number);
				continue;
				//change the times for the most recent media object
			}
			if (line.find("colour_l:") != std::string::npos) {
				line.replace(0, 10, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_pLightManager->vecLights[lightIndex].diffuse.x = stof(number);
						number = "";
					}
					else
					{
						::g_pLightManager->vecLights[lightIndex].diffuse.y = stof(number);
						number = "";
					}
				}
				::g_pLightManager->vecLights[lightIndex].diffuse.z = stof(number);
				continue;
				//change the times for the most recent media object
			}
		}

	}
	catch (std::exception ex)
	{
		return false;
	}
	return true;
}

