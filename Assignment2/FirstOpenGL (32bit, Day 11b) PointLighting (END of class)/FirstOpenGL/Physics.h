#ifndef _Physics_HG_
#define _Physics_HG_

//forward declare
class cGameObject;

// Our objects are vectors of pointers, so we might as well pass pointers
bool PenetrationTestSphereSphere( cGameObject* pA, cGameObject* pB );


#endif
