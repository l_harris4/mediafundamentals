#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>			// C++ cin, cout, etc.
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr
#include <stdlib.h>
#include <stdio.h>
// Add the file stuff library (file stream>
#include <fstream>
#include <sstream>		// "String stream"
#include <string>
#include <time.h>

#include <vector>		//  smart array, "array" in most languages
#include "Utilities.h"
#include "ModelUtilities.h"
#include "cMesh.h"
#include "cShaderManager.h" 
#include "cGameObject.h"
#include "cVAOMeshManager.h"

#include "Physics.h"	// Physics collision detection functions

#include "cLightManager.h"
#include "cSoundPlayer.h"

// This is the function "signature" for the 
//	function WAY at the bottom of the file.
void LightFlicker(double curTime);

bool LoadModelsLightsFromFile(void);
void SaveDataToFile(void);
bool Load3DModelsIntoMeshManager(int shaderID, cVAOMeshManager* pVAOManager, std::string &error);

std::vector< cGameObject* >  g_vecGameObjects;

int g_selectedGameObjectIndex = 0;
int g_selectedLightIndex = 0;
int partySection = 0;
int friendId = 1;
bool g_movingPlayer = true;
bool g_lightsOn = true;
bool g_movingLights = false;
const float MOVESPEED = 0.7f;
const float ROTATIONSPEED = -2;
const float CAMERASPEED = 0.2f;
const float FIRSTROOMEND = 72.9;
const float SECONDROOMEND = 2.9;


glm::vec3 g_cameraXYZ = glm::vec3(0.0f, 0.0f, 60.0f);
glm::vec3 g_cameraTarget_XYZ = glm::vec3(0.0f, 0.0f, 0.0f);

cVAOMeshManager* g_pVAOManager = 0;	

cShaderManager*		g_pShaderManager;
cLightManager*		g_pLightManager;
cSoundPlayer*       g_pSoundManager;

void rotate_point(float c1, float c2, float angle, float &point1, float &point2)
{
	angle = glm::radians(angle);
	float rotated1 = cos(angle) * (point1 - c1) - sin(angle) * (point2 - c2) + c1;
	float rotated2 = sin(angle) * (point1 - c1) + cos(angle) * (point2 - c2) + c2;

	point1 = rotated1;
	point2 = rotated2;
}

static void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);

	const float CAMERASPEED = 0.1f;
	switch (key)
	{
	case GLFW_KEY_U://cycle through moving friends or moving yourself
		if (action == GLFW_PRESS)
		{
			g_movingPlayer = !g_movingPlayer;
		}
		break;
	case GLFW_KEY_I://cycle between which friend you are moving
		if (action == GLFW_PRESS)
		{
			friendId++;
			if (friendId > 3)
			{
				friendId = 1;
			}
		}
		break;
	case GLFW_KEY_A:// Move your character/friend left
		if (::g_movingPlayer)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->position.x -= MOVESPEED;
			g_pSoundManager->UpdateListenerPos(::g_vecGameObjects[::g_selectedGameObjectIndex]->position);
		}
		else
		{
			::g_vecGameObjects[friendId]->position.x -= MOVESPEED;
			g_pSoundManager->UpdateSoundPos(friendId, ::g_vecGameObjects[friendId]->position);
		}
		break;
	case GLFW_KEY_D:// Move your character/friend right
		if (::g_movingPlayer)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->position.x += MOVESPEED;
			g_pSoundManager->UpdateListenerPos(::g_vecGameObjects[::g_selectedGameObjectIndex]->position);
		}
		else
		{
			::g_vecGameObjects[friendId]->position.x += MOVESPEED;
			g_pSoundManager->UpdateSoundPos(friendId, ::g_vecGameObjects[friendId]->position);
		}
		break;
	case GLFW_KEY_W:// Move your character/friend upstage
		if (::g_movingPlayer)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z -= MOVESPEED;

			int tempPartySection = 0;
			if (::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z < FIRSTROOMEND &&
				::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z > SECONDROOMEND)
			{
				tempPartySection = 1;
			}
			else if (::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z <= SECONDROOMEND)
			{
				tempPartySection = 2;
			}
			else if (::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z >= FIRSTROOMEND)
			{
				tempPartySection = 0;
			}

			if (tempPartySection != partySection)
			{
				g_pSoundManager->ChangeSelectedSection(tempPartySection);
				partySection = tempPartySection;
			}
			g_pSoundManager->UpdateListenerPos(::g_vecGameObjects[::g_selectedGameObjectIndex]->position);
		}
		else
		{
			::g_vecGameObjects[friendId]->position.z -= MOVESPEED;
			g_pSoundManager->UpdateSoundPos(friendId, ::g_vecGameObjects[friendId]->position);
		}
		break;
	case GLFW_KEY_S:// Move your character/friend downstage
		if (::g_movingPlayer)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z += MOVESPEED;

			int tempPartySection = 0;
			if (::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z < FIRSTROOMEND &&
				::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z > SECONDROOMEND)
			{
				tempPartySection = 1;
			}
			else if (::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z <= SECONDROOMEND)
			{
				tempPartySection = 2;
			}
			else if (::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z >= FIRSTROOMEND)
			{
				tempPartySection = 0;
			}

			if (tempPartySection != partySection)
			{
				g_pSoundManager->ChangeSelectedSection(tempPartySection);
				partySection = tempPartySection;
			}
			g_pSoundManager->UpdateListenerPos(::g_vecGameObjects[::g_selectedGameObjectIndex]->position);
		}
		else
		{
			::g_vecGameObjects[friendId]->position.z += MOVESPEED;
			g_pSoundManager->UpdateSoundPos(friendId, ::g_vecGameObjects[friendId]->position);
		}
		break;

	case GLFW_KEY_P: //used for debugging, printing the current location of object
		if (::g_movingPlayer)
		{
			std::cout << "x:" << ::g_vecGameObjects[::g_selectedGameObjectIndex]->position.x
				<< " y:" << ::g_vecGameObjects[::g_selectedGameObjectIndex]->position.y
				<< " z:" << ::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z
				<< std::endl;

			std::cout << "ox:" << glm::degrees(::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.x)
				<< " oy:" << glm::degrees(::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.y)
				<< " oz:" << glm::degrees(::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.z)
				<< std::endl;
		}
		else if (::g_movingLights)
		{
			std::cout << "x:" << ::g_pLightManager->vecLights[::g_selectedLightIndex].position.x
				<< " y:" << ::g_pLightManager->vecLights[::g_selectedLightIndex].position.y
				<< " z:" << ::g_pLightManager->vecLights[::g_selectedLightIndex].position.z
				<< std::endl;
		}
		break;


	//1 - 6 are used to change the rotation of an object
	case GLFW_KEY_1:
		if (action == GLFW_PRESS)
		{
			g_pSoundManager->DSPEffects(1);
		}
		break;
	case GLFW_KEY_2:
		if (action == GLFW_PRESS)
		{
			g_pSoundManager->DSPEffects(2);
		}
		break;
	case GLFW_KEY_3:
		if (action == GLFW_PRESS)
		{
			g_pSoundManager->DSPEffects(3);
		}
		break;
	case GLFW_KEY_4:
		if (action == GLFW_PRESS)
		{
			g_pSoundManager->DSPEffects(4);
		}
		break;
	case GLFW_KEY_5:
		if (action == GLFW_PRESS)
		{
			g_pSoundManager->DSPEffects(5);
		}
		break;
	case GLFW_KEY_6:
		if (action == GLFW_PRESS)
		{
			g_pSoundManager->DSPEffects(6);
		}
		break;
	case GLFW_KEY_7:
		if (action == GLFW_PRESS)
		{
			g_pSoundManager->DSPEffects(7);
		}
		break;
	case GLFW_KEY_8:
		if (action == GLFW_PRESS)
		{
			g_pSoundManager->DSPEffects(8);
		}
		break;
	case GLFW_KEY_9:
		if (action == GLFW_PRESS)
		{
			g_pSoundManager->DSPEffects(9);
		}
		break;

	case GLFW_KEY_UP: //Change the selected attribute
	{
		if (action == GLFW_PRESS)
		{
			g_pSoundManager->ChangeAttributeUp();
		}
	}
		break;
	case GLFW_KEY_DOWN: //Change the selected attribute
	{
		if (action == GLFW_PRESS)
		{
			g_pSoundManager->ChangeAttributeDown();
		}
		
	}
		break;
	case GLFW_KEY_LEFT: //Change the selected attribute
	{
		if (action == GLFW_PRESS)
		{
			g_pSoundManager->ChangeSelectedDown();
		}
		
	}
	break;
	case GLFW_KEY_RIGHT: //Change the selected attribute
	{
		if (action == GLFW_PRESS)
		{
			g_pSoundManager->ChangeSelectedUp();
		}
		
	}
	break;

	case GLFW_KEY_J: //Change the selected attribute
	{
		if (action == GLFW_PRESS)
		{
			g_pSoundManager->ModifyAttributeDown();
		}

	}
	break;

	case GLFW_KEY_K: //Change the selected attribute
	{
		if (action == GLFW_PRESS)
		{
			g_pSoundManager->ModifyAttributeUp();
		}

	}
	break;

	case GLFW_KEY_ENTER: //Change the selected attribute
	{
		if (action == GLFW_PRESS)
		{
			g_pSoundManager->changingSoundMode += 1;
			if (g_pSoundManager->changingSoundMode >= 3)
			{
				g_pSoundManager->changingSoundMode = 0;
			}
		}

	}
	break;

	}
	return;
}


int main(void)
{

	GLFWwindow* window;
	GLint mvp_location;	// , vpos_location, vcol_location;
	glfwSetErrorCallback(error_callback);

	// Other uniforms:
	GLint uniLoc_materialDiffuse = -1;
	GLint uniLoc_materialAmbient = -1;
	GLint uniLoc_ambientToDiffuseRatio = -1; 	// Maybe	// 0.2 or 0.3
	GLint uniLoc_materialSpecular = -1;  // rgb = colour of HIGHLIGHT only
								// w = shininess of the 
	GLint uniLoc_eyePosition = -1;	// Camera position
	GLint uniLoc_mModel = -1;
	GLint uniLoc_mView = -1;
	GLint uniLoc_mProjection = -1;


	if (!glfwInit())
		exit(EXIT_FAILURE);

	int height = 480;	/* default */
	int width = 640;	// default
	std::string title = "OpenGL Rocks";

	std::ifstream infoFile("config.txt");
	if (!infoFile.is_open())
	{	// File didn't open...
		std::cout << "Can't find config file" << std::endl;
		std::cout << "Using defaults" << std::endl;
	}
	else
	{	// File DID open, so read it... 
		std::string a;

		infoFile >> a;	// "Game"	//std::cin >> a;
		infoFile >> a;	// "Config"
		infoFile >> a;	// "width"

		infoFile >> width;	// 1080

		infoFile >> a;	// "height"

		infoFile >> height;	// 768

		infoFile >> a;		// Title_Start

		std::stringstream ssTitle;		// Inside "sstream"
		bool bKeepReading = true;
		do
		{
			infoFile >> a;		// Title_End??
			if (a != "Title_End")
			{
				ssTitle << a << " ";
			}
			else
			{	// it IS the end! 
				bKeepReading = false;
				title = ssTitle.str();
			}
		} while (bKeepReading);


	}//if ( ! infoFile.is_open() )




	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	window = glfwCreateWindow(width, height,
		title.c_str(),
		NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(window, key_callback);
	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glfwSwapInterval(1);

	std::cout << glGetString(GL_VENDOR) << " "
		<< glGetString(GL_RENDERER) << ", "
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << "Shader language version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;


	::g_pShaderManager = new cShaderManager();

	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	vertShader.fileName = "simpleVert.glsl";
	fragShader.fileName = "simpleFrag.glsl";

	::g_pShaderManager->setBasePath("assets//shaders//");

	// Shader objects are passed by reference so that
	//	we can look at the results if we wanted to. 
	if (!::g_pShaderManager->createProgramFromFile(
		"mySexyShader", vertShader, fragShader))
	{
		std::cout << "Oh no! All is lost!!! Blame Loki!!!" << std::endl;
		std::cout << ::g_pShaderManager->getLastError() << std::endl;
		// Should we exit?? 
		return -1;
		//		exit(
	}
	std::cout << "The shaders comipled and linked OK" << std::endl;


	// Load models
	::g_pVAOManager = new cVAOMeshManager();

	GLint sexyShaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	std::string error;
	if (!Load3DModelsIntoMeshManager(sexyShaderID, ::g_pVAOManager, error))
	{
		std::cout << "Not all models were loaded..." << std::endl;
		std::cout << error << std::endl;
	}



	GLint currentProgID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	// Get the uniform locations for this shader
	mvp_location = glGetUniformLocation(currentProgID, "MVP");		// program, "MVP");
	uniLoc_materialDiffuse = glGetUniformLocation(currentProgID, "materialDiffuse");
	uniLoc_materialAmbient = glGetUniformLocation(currentProgID, "materialAmbient");
	uniLoc_ambientToDiffuseRatio = glGetUniformLocation(currentProgID, "ambientToDiffuseRatio");
	uniLoc_materialSpecular = glGetUniformLocation(currentProgID, "materialSpecular");
	uniLoc_eyePosition = glGetUniformLocation(currentProgID, "eyePosition");

	uniLoc_mModel = glGetUniformLocation(currentProgID, "mModel");
	uniLoc_mView = glGetUniformLocation(currentProgID, "mView");
	uniLoc_mProjection = glGetUniformLocation(currentProgID, "mProjection");

	//	GLint uniLoc_diffuseColour = glGetUniformLocation( currentProgID, "diffuseColour" );

	::g_pLightManager = new cLightManager();
	::g_pSoundManager = new cSoundPlayer();

	LoadModelsLightsFromFile();
	::g_pLightManager->LoadShaderUniformLocations(currentProgID);

	glEnable(GL_DEPTH);

	// Gets the "current" time "tick" or "step"
	double lastTimeStep = glfwGetTime();

	// Main game or application loop
	while (!glfwWindowShouldClose(window))
	{
		float ratio;
		int width, height;
		glm::mat4x4 m, p, mvp;			//  mat4x4 m, p, mvp;

		glfwGetFramebufferSize(window, &width, &height);
		ratio = width / (float)height;
		glViewport(0, 0, width, height);

		// Clear colour AND depth buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//glEnable(GL_DEPTH_TEST);

		
		g_pSoundManager->UpdateSystem();


		// Update all the light uniforms...
		// (for the whole scene)
		::g_pLightManager->CopyLightInformationToCurrentShader();


		unsigned int sizeOfVector = ::g_vecGameObjects.size();	//*****//
		for (int index = 0; index != sizeOfVector; index++)
		{

			// Is there a game object? 
			if (::g_vecGameObjects[index] == 0)	//if ( ::g_GameObjects[index] == 0 )
			{	// Nothing to draw
				continue;		// Skip all for loop code and go to next
			}

			// Was near the draw call, but we need the mesh name
			std::string meshToDraw = ::g_vecGameObjects[index]->meshName;		//::g_GameObjects[index]->meshName;

			sVAOInfo VAODrawInfo;
			if (::g_pVAOManager->lookupVAOFromName(meshToDraw, VAODrawInfo) == false)
			{	// Didn't find mesh
				continue;
			}



			// There IS something to draw

			m = glm::mat4x4(1.0f);	//		mat4x4_identity(m);

			glm::mat4 matRreRotZ = glm::mat4x4(1.0f);
			matRreRotZ = glm::rotate(matRreRotZ, ::g_vecGameObjects[index]->orientation.z,
				glm::vec3(0.0f, 0.0f, 1.0f));
			m = m * matRreRotZ;

			glm::mat4 trans = glm::mat4x4(1.0f);
			trans = glm::translate(trans,
				::g_vecGameObjects[index]->position);
			m = m * trans;

			glm::mat4 matPostRotZ = glm::mat4x4(1.0f);
			matPostRotZ = glm::rotate(matPostRotZ, ::g_vecGameObjects[index]->orientation2.z,
				glm::vec3(0.0f, 0.0f, 1.0f));
			m = m * matPostRotZ;

			glm::mat4 matPostRotY = glm::mat4x4(1.0f);
			matPostRotY = glm::rotate(matPostRotY, ::g_vecGameObjects[index]->orientation2.y,
				glm::vec3(0.0f, 1.0f, 0.0f));
			m = m * matPostRotY;


			glm::mat4 matPostRotX = glm::mat4x4(1.0f);
			matPostRotX = glm::rotate(matPostRotX, ::g_vecGameObjects[index]->orientation2.x,
				glm::vec3(1.0f, 0.0f, 0.0f));
			m = m * matPostRotX;

			float finalScale = ::g_vecGameObjects[index]->scale;

			glm::mat4 matScale = glm::mat4x4(1.0f);
			matScale = glm::scale(matScale,
				glm::vec3(finalScale,
					finalScale,
					finalScale));
			m = m * matScale;

			p = glm::perspective(0.6f,			// FOV
				ratio,		// Aspect ratio
				0.1f,			// Near (as big as possible)
				1000.0f);	// Far (as small as possible)

			// View or "camera" matrix
			glm::mat4 v = glm::mat4(1.0f);	// identity
			g_cameraTarget_XYZ = ::g_vecGameObjects[::g_selectedGameObjectIndex]->position;
			g_cameraXYZ = glm::vec3(g_cameraTarget_XYZ.x, g_cameraTarget_XYZ.y +20, g_cameraTarget_XYZ.z + 20);

			v = glm::lookAt(g_cameraXYZ,						// "eye" or "camera" position
				g_cameraTarget_XYZ,		// "At" or "target" 
				glm::vec3(0.0f, 1.0f, 0.0f));	// "up" vector


			::g_pShaderManager->useShaderProgram("mySexyShader");
			GLint shaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

			glUniformMatrix4fv(uniLoc_mModel, 1, GL_FALSE,
				(const GLfloat*)glm::value_ptr(m));
			glUniformMatrix4fv(uniLoc_mView, 1, GL_FALSE,
				(const GLfloat*)glm::value_ptr(v));
			glUniformMatrix4fv(uniLoc_mProjection, 1, GL_FALSE,
				(const GLfloat*)glm::value_ptr(p));

			glm::mat4 mWorldInTranpose = glm::inverse(glm::transpose(m));

			glUniform4f(uniLoc_materialDiffuse,
				::g_vecGameObjects[index]->diffuseColour.r,
				::g_vecGameObjects[index]->diffuseColour.g,
				::g_vecGameObjects[index]->diffuseColour.b,
				::g_vecGameObjects[index]->diffuseColour.a);

			if (::g_vecGameObjects[index]->bIsWireframe)
			{
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			}
			else
			{
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);	// Default
			}
			glEnable(GL_DEPTH_TEST);		// Test for z and store in z buffer
			glCullFace(GL_BACK);


			glBindVertexArray(VAODrawInfo.VAO_ID);

			glDrawElements(GL_TRIANGLES,
				VAODrawInfo.numberOfIndices,
				GL_UNSIGNED_INT,		
				0);

			glBindVertexArray(0);

		}//for ( int index = 0...


		std::stringstream ssTitle;
		ssTitle << "Camera (xyz): "
			<< g_cameraXYZ.x << ", "
			<< g_cameraXYZ.y << ", "
			<< g_cameraXYZ.z;
		glfwSetWindowTitle(window, ssTitle.str().c_str());

		glfwSwapBuffers(window);
		glfwPollEvents();


		// Essentially the "frame time"
		// Now many seconds that have elapsed since we last checked
		double curTime = glfwGetTime();
		double deltaTime = curTime - lastTimeStep;

		//LightFlicker(curTime);

		lastTimeStep = curTime;

	}// while ( ! glfwWindowShouldClose(window) )


	glfwDestroyWindow(window);
	glfwTerminate();

	//Cleaning up memory
	int loopSize = ::g_vecGameObjects.size();
	for (int objectIndex = 0; objectIndex < loopSize; objectIndex++)
	{
		delete ::g_vecGameObjects[objectIndex];
	}

	//deleting managers
	delete ::g_pShaderManager;
	delete ::g_pVAOManager;
	delete ::g_pLightManager;
	delete ::g_pSoundManager;

	return 0;
}

void LightFlicker(double curTime)
{
	srand(time(NULL));
	curTime *= 10;
	int randNum = rand() % 2;
	if ((int)curTime % 6 > 3 && ::g_pLightManager->vecLights.size() > 1)
	{
		if (randNum == 1)
		{
			::g_pLightManager->vecLights.back().diffuse = glm::vec3(0.0f, 0.0f, 0.0f);
		}
		else
		{
			::g_pLightManager->vecLights[::g_pLightManager->vecLights.size()-2].diffuse = 
				glm::vec3(0.0f, 0.0f, 0.0f);
		}
	}
	else
	{
		if (randNum == 1)
		{
			::g_pLightManager->vecLights.back().diffuse = glm::vec3(1.0f, 0.1f, 0.1f);
		}
		else
		{
			::g_pLightManager->vecLights[::g_pLightManager->vecLights.size() - 2].diffuse = 
				glm::vec3(1.0f, 0.1f, 0.1f);
		}
	}
}


