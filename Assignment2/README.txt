------Please run the application from visual studio in debug mode.

Use the platform toolset 141

Use Multi-Byte Character set

Use Visual Studio 2017

Use Debug

Use x86

Code: 
	
	All of the code that you will need to see is in cSoundPlayer.cpp, cSoundPlayer.h, and Main.cpp.

Story:

	You are at a party when you would rather not be. Your objective is to find your two 
	friends and get them to your friend waiting outside. One of your friends is having a laughing fit,
	another is crying, and finally one is whistling. Use those sounds to find your friends in the space.
	You and your friends are all represented by coloured rectangular prisms.
	
Controls:

	U: toggles between controlling you or a friend.
	I: switches which friend you are controlling.
	W,A,S,D: control the player/friends movement.
	1-9: toggles dps effects.
	Enter: toggles between changing all sounds or one particular sound.
	Up,Down: changes which attribute of a sound is currently able to be changed
	Left,Right: change which sound file is currently being edited
	J,K: affect the current attribute for the current sound. ie: change volume up
	
The 1st,2nd, and 3rd groups of sounds are in the 1st, 2nd, and 3rd rooms respectively.
As the listener switches rooms, so does the group that is currently playing.

Note: the sounds of the 3 friends are in a fourth group that is set to be unaffected by the dps effects.

Note: when changing all sounds it affects the master channel group, when changing single sounds it only
changes the sound you have selected, and finally when changing specific group sounds, it changes the sounds
of the room you are in, aside from the 3d sounds as I did not bind them to specific rooms of the model.


	