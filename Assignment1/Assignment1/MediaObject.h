#ifndef _MEDIAOBJECT_HG_
#define _MEDIAOBJECT_HG_

#include <fmod.hpp>
#include <fmod_errors.h>
#include <string>

class MediaObject
{
public:
	//constructor
	MediaObject();
	//destructor
	~MediaObject();
	//member variables
	FMOD::Sound* sound;
	FMOD::Channel* channel;
	bool streamObject;
	std::string fileName;
	float pan;
	float volume;
	float frequency;
	float pitch;
	float playBackSpeed;
};


#endif // !_MEDIAOBJECT_HG_
