#include "MediaObject.h"
#include <vector>

class Scene
{
public:
	//constructor
	Scene();
	//destructor
	~Scene();
	//member variables
	std::vector<MediaObject*> mediaObjects;

	//the pair is the index of the media object and the time that it will be played
	std::vector<std::pair<int, int>> mediaObjectTimingLinks;
	std::string prompt;
	int length;
	//methods
	void Startup();
	void CheckIfSoundStarts(int tick, FMOD::System * msystem);
	void Stop();
};

