#include "Scene.h"

//constructor
Scene::Scene()
{
	prompt = "";
	length = 0;
}

//destructor
Scene::~Scene()
{
	for (int i = 0; i < mediaObjects.size(); i++)
	{
		delete mediaObjects[i];
	}

	mediaObjects.clear();
}

//starts up the ambient background sound for the scene
void Scene::Startup()
{
	//If there is background music/ambient for the scene start that up
	if (mediaObjects.front()->streamObject)
	{
		FMOD_RESULT result = mediaObjects.front()->channel->setPaused(false);
		if (result != FMOD_OK) {
			fprintf(stderr, "FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
			exit(-1);
		}
	}
}

void Scene::CheckIfSoundStarts(int tick, FMOD::System * msystem)
{
	//If there is a sound effect to be played at this moment, then play it
	for(int soundIndex = 0; soundIndex < mediaObjectTimingLinks.size(); ++ soundIndex)
	//while (lastSamplePlayedIndex < mediaObjectTimingLinks.size() && tick == mediaObjectTimingLinks[lastSamplePlayedIndex].second)
	{
		if (tick == mediaObjectTimingLinks[soundIndex].second) 
		{
			//check if it is a stream or sample sound
			//if it is a stream then simply unpause if it is paused
			if (mediaObjects[mediaObjectTimingLinks[soundIndex].first]->streamObject)
			{
				mediaObjects[mediaObjectTimingLinks[soundIndex].first]->channel->setPaused(false);
			}
			else
			{
				//if it is a sample sound then check if it is playing or not,
				bool playing = true;
				mediaObjects[mediaObjectTimingLinks[soundIndex].first]->channel->isPlaying(&playing);
				bool paused = false;
				mediaObjects[mediaObjectTimingLinks[soundIndex].first]->channel->getPaused(&paused);
				//if it is playing then do nothing
				//if it isn't then play sound again (the settings should carry over)
				if (playing && paused)
				{
					mediaObjects[mediaObjectTimingLinks[soundIndex].first]->channel->setPaused(false);
				}
				else if (!playing)
				{

					//play the sound again
					msystem->playSound(mediaObjects[mediaObjectTimingLinks[soundIndex].first]->sound,
						0, false, &mediaObjects[mediaObjectTimingLinks[soundIndex].first]->channel);

					//set the old settings
					mediaObjects[mediaObjectTimingLinks[soundIndex].first]->channel->
						setFrequency(mediaObjects[mediaObjectTimingLinks[soundIndex].first]->frequency);
					mediaObjects[mediaObjectTimingLinks[soundIndex].first]->channel->
						setPan(mediaObjects[mediaObjectTimingLinks[soundIndex].first]->pan);
					mediaObjects[mediaObjectTimingLinks[soundIndex].first]->channel->
						setVolume(mediaObjects[mediaObjectTimingLinks[soundIndex].first]->volume);
					mediaObjects[mediaObjectTimingLinks[soundIndex].first]->channel->
						setPitch(mediaObjects[mediaObjectTimingLinks[soundIndex].first]->pitch);

					mediaObjects[mediaObjectTimingLinks[soundIndex].first]->channel->setPaused(false);
				}
			}
		}
	}
}

//stop all the sounds in the scene
void Scene::Stop()
{
	FMOD_RESULT result;
	int size = mediaObjects.size();
	for (int mediaObjectIndex = 0; mediaObjectIndex < size; mediaObjectIndex++)
	{
		result = mediaObjects[mediaObjectIndex]->channel->setPaused(true);
	}
}
