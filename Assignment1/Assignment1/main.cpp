#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>

#include "Scene.h"
#include <stdio.h>
#include <Windows.h>
#include "utils.h"


using namespace std;

//global variables
FMOD::System *g_psystem = NULL;
vector<Scene*> g_scenes;
FMOD_RESULT g_result;
bool g_keydown = false;
bool g_mis_esc = false;
int g_currentSelectedSound = 0;
int g_currentSelectedSoundScene = 0;
int g_currentTick = 0;
int g_currentTickInScene = 0;
int g_totalTicks = 0;
int g_masterSceneIndex = 0;
bool g_compressedSelected = false;
bool g_changingAllSounds = true;
string g_baseSoundFilePath = "../Sounds/";

enum eChangingAttribute {
	VOLUME = 0,
	BALANCE,
	PITCH,
	PLAYBACKSPEED,
	FREQUENCY
};

eChangingAttribute changingAttribute = eChangingAttribute::VOLUME;

//a method to check if an error has occured
void checkForErrors() {
	if (::g_result != FMOD_OK) {
		fprintf(stderr, "FMOD error! (%d) %s\n", ::g_result, FMOD_ErrorString(::g_result));
		exit(-1);
	}
}

//for initliazing fmod
void init_fmod() {

	// Create the main system object.
	::g_result = FMOD::System_Create(&::g_psystem);
	checkForErrors();

	//Initializes the system object, and the msound device. This has to be called at the start of the user's program
	::g_result = ::g_psystem->init(512, FMOD_INIT_NORMAL, 0);    // Initialize FMOD.
	checkForErrors();

}

//a custom compare used later for sorting a vector of pairs
bool pairCompare(const pair<int, int>& firstElem, const pair<int, int>& secondElem) {
	return firstElem.second < secondElem.second;
}

//converting the code into a readable string
string convertSoundFormatCode(int code) {
	switch (code) {
	case 0:
		return " NONE";
		break;
	case 1:
		return " PCM8";
		break;
	case 2:
		return " PCM16";
		break;
	case 3:
		return " PCM24";
		break;
	case 4:
		return " PCM32";
		break;
	case 5:
		return " PCMFLOAT";
		break;
	case 6:
		return " BITSTREAM";
		break;
	case 7:
		return " MAX";
		break;
	case 65536:
		return " FORCEINT";
		break;
	}
}

//converting the code into a readable string
string convertSoundTypeCode(int code) {

	switch (code) {
	case 0:
		return " UNKNOWN";
		break;
	case 1:
		return " AIFF";
		break;
	case 2:
		return " ASF";
		break;
	case 3:
		return " DLS";
		break;
	case 4:
		return " FLAC";
		break;
	case 5:
		return " FSB";
		break;
	case 6:
		return " IT";
		break;
	case 7:
		return " MIDI";
		break;
	case 8:
		return " MOD";
		break;
	case 9:
		return " MPEG";
		break;
	case 10:
		return " OGGVORBIS";
		break;
	case 11:
		return " PLAYLIST";
		break;
	case 12:
		return " RAW";
		break;
	case 13:
		return " S3M";
		break;
	case 14:
		return " USER";
		break;
	case 15:
		return " WAV";
		break;
	case 16:
		return " XM";
		break;
	case 17:
		return " XMA";
		break;
	case 18:
		return " AUDIOQUEUE";
		break;
	case 19:
		return " AT9";
		break;
	case 20:
		return " VORBIS";
		break;
	case 21:
		return " MEDIA_FOUNDATION";
		break;
	case 22:
		return " MEDIACODEC";
		break;
	case 23:
		return " FADPCM";
		break;
	case 24:
		return " MAX";
		break;
	}
}

//a method to load all the data in from a file
bool LoadSoundsFromFile()
{
	string filename = "SoundsList.txt";
	string line;
	ifstream inFile(filename.c_str());
	Scene tempScene;
	MediaObject tempMediaObject;
	bool inScene = false;
	bool inSample = false;

	try {
		while (getline(inFile, line))
		{
			if (line == "")
			{
				continue;
			}
			if (line == "Section Start") {
				Scene* newScene = new Scene(tempScene);
				::g_scenes.push_back(newScene);
				inScene = true;
				continue;
			}
			if (line.find("prompt:") != string::npos) {
				line.replace(0, 8, "");
				::g_scenes.back()->prompt = line;
				continue;
			}
			if (line.find("length:") != string::npos) {
				line.replace(0, 8, "");
				::g_scenes.back()->length = stoi(line);
				continue;
			}
			if (line.find("stream:") != string::npos) {
				//create a new media object and load that into the scene
				line.replace(0, 8, "");
				FMOD::Sound* sound1;
				FMOD::Channel* channel1 = 0;
				line = ::g_baseSoundFilePath + line.append(::g_compressedSelected ? ".mp3" : ".wav");
				::g_result = ::g_psystem->createSound(line.c_str(), FMOD_CREATESTREAM, 0, &sound1);
				checkForErrors();

				::g_result = sound1->setMode(FMOD_LOOP_NORMAL);
				checkForErrors();

				::g_result = ::g_psystem->playSound(sound1, 0, true, &channel1);
				checkForErrors();

				channel1->setVolume(0.2f);
				checkForErrors();

				//assign sound and channel to the temp media object
				tempMediaObject.sound = sound1;
				tempMediaObject.channel = channel1;
				//make a new mediaobject
				MediaObject* mediaObject = new MediaObject(tempMediaObject);
				mediaObject->streamObject = true;
				mediaObject->fileName = line;
				mediaObject->volume = 0.2f;
				//put the media object on the backest scene object
				::g_scenes.back()->mediaObjects.push_back(mediaObject);
				continue;
			}
			if (line.find("name:") != string::npos) {
				//create a new media object and load that into the scene
				line.replace(0, 6, "");
				//YOUR CODE HERE
				FMOD::Sound* sound1;
				FMOD::Channel* channel1 = 0;
				line = ::g_baseSoundFilePath + line;
				::g_result = ::g_psystem->createSound(line.c_str(), FMOD_CREATESAMPLE, 0, &sound1);
				checkForErrors();

				//Here is where the sound is set to play once
				::g_result = sound1->setMode(FMOD_LOOP_OFF);
				checkForErrors();

				::g_result = ::g_psystem->playSound(sound1, 0, true, &channel1);
				checkForErrors();

				//assign sound and channel to the temp media object
				tempMediaObject.sound = sound1;
				tempMediaObject.channel = channel1;
				//make a new mediaobject
				MediaObject* mediaObject = new MediaObject(tempMediaObject);
				mediaObject->fileName = line;
				//put the media object on the backest scene object
				::g_scenes.back()->mediaObjects.push_back(mediaObject);
				continue;
			}
			if (line.find("timesplayed:") != string::npos) {
				line.replace(0, 13, "");
				continue;
				//change the timesplayed for the most recent media object
			}
			if (line.find("times:") != string::npos) {
				line.replace(0, 7, "");
				string number;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else
					{
						//tranform the number into the number, 
						//Then load the number along with the index of the last media object into
						//    the mediaObjectTimingLinks vector of the scene
						::g_scenes.back()->mediaObjectTimingLinks.push_back(make_pair(::g_scenes.back()->mediaObjects.size() - 1, stoi(number)));
						number = "";
					}
				}
				::g_scenes.back()->mediaObjectTimingLinks.push_back(make_pair(::g_scenes.back()->mediaObjects.size() - 1, stoi(number)));
				continue;
				//change the times for the most recent media object
			}
			if (line == "Section End") {
				tempScene.mediaObjects.clear();
				continue;
			}
		}

		//for each scene, sort the pairs of timings
		for (int sceneIndex = 0; sceneIndex < ::g_scenes.size(); sceneIndex++)
		{
			sort(::g_scenes[sceneIndex]->mediaObjectTimingLinks.begin(), ::g_scenes[sceneIndex]->mediaObjectTimingLinks.end(), pairCompare);
		}
	}
	catch (...)
	{
		return false;
	}
	return true;
}

void handle_keyboard() {

	//Esc key pressed
	if (GetAsyncKeyState(VK_ESCAPE)) {
		::g_mis_esc = true;
	}

	//===============================================================================================
	//Arrow UP
	else if ((GetKeyState(VK_UP) < 0) && !::g_keydown) {
		::g_keydown = true;
		//This will change the current sound that is being displayed, by putting it up
		if (::g_currentSelectedSoundScene == ::g_scenes.size() - 1 && ::g_currentSelectedSound == ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects.size() - 1)
		{
			::g_currentSelectedSound = 0;
			::g_currentSelectedSoundScene = 0;
		}
		else if (::g_currentSelectedSound == ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects.size() - 1)
		{
			::g_currentSelectedSound = 0;
			::g_currentSelectedSoundScene++;
		}
		else
		{
			::g_currentSelectedSound++;
		}
		Sleep(200);
		::g_keydown = false;
	}

	//Arrow Down
	else if ((GetKeyState(VK_DOWN) < 0) && !::g_keydown) {
		::g_keydown = true;
		//This will change the current sound that is being displayed, by putting it down
		if (::g_currentSelectedSoundScene == 0 && ::g_currentSelectedSound == 0)
		{
			::g_currentSelectedSoundScene = ::g_scenes.size() - 1;
			::g_currentSelectedSound = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects.size()-1;
		}
		else if (::g_currentSelectedSound == 0)
		{
			::g_currentSelectedSoundScene--;
			::g_currentSelectedSound = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects.size() - 1;
		}
		else
		{
			::g_currentSelectedSound--;
		}

		Sleep(200);
		::g_keydown = false;
	}

	//===============================================================================================
	//Arrow right  //Pressing this will further the scene
	else if ((GetKeyState(VK_RIGHT) < 0) && !::g_keydown) {
		::g_keydown = true;

		if (::g_currentTick < ::g_totalTicks)
			::g_currentTick++;

		//if the currentTickInScene is greater than the length of the scene
		//then change the masterSceneIndex and reset the currentTickInScene
		if (::g_currentTickInScene > ::g_scenes[::g_masterSceneIndex]->length && ::g_masterSceneIndex < ::g_scenes.size()-1)
		{
			::g_masterSceneIndex++;
			::g_currentTickInScene = 0;
			//call a method on the current scene that starts up the background
			if (::g_masterSceneIndex < ::g_scenes.size())
				::g_scenes[::g_masterSceneIndex]->Startup();

			//call a method on the previous scene that stops all the sounds
			::g_scenes[::g_masterSceneIndex - 1]->Stop();

			int temptick = 0;
			for (int i = 0; i < ::g_masterSceneIndex; i++)
			{
				temptick += ::g_scenes[i]->length;
			}
			::g_currentTick = temptick;
		}
		else if (::g_currentTickInScene <= ::g_scenes[::g_masterSceneIndex]->length) {
			::g_currentTickInScene++;
		}
		//call a method on the current scene that checks the tick count and sees whether a sound should
		//start or not
		::g_scenes[::g_masterSceneIndex]->CheckIfSoundStarts(::g_currentTickInScene, ::g_psystem);


		Sleep(30);
		::g_keydown = false;
	}

	//===============================================================================================
	//Arrow left  //Pressing this will rewind the scene by 1 tick
	else if ((GetKeyState(VK_LEFT) < 0) && !::g_keydown) {
		::g_keydown = true;

		if (::g_currentTick > 0)
			::g_currentTick--;

		//if the currentTickInScene is greater than the length of the scene
		//then change the masterSceneIndex and reset the currentTickInScene
		if (::g_currentTickInScene == 0 && ::g_masterSceneIndex != 0)
		{
			::g_masterSceneIndex--;
			::g_currentTickInScene = ::g_scenes[::g_masterSceneIndex]->length;
			//call a method on the current scene that starts up the background
			if (::g_masterSceneIndex < ::g_scenes.size())
				::g_scenes[::g_masterSceneIndex]->Startup();

			//call a method on the previous scene that stops all the sounds
			::g_scenes[::g_masterSceneIndex + 1]->Stop();

			int temptick = 0;
			for (int i = 0; i <= ::g_masterSceneIndex; i++)
			{
				temptick += ::g_scenes[i]->length;
			}
			::g_currentTick = temptick;
		}
		else if (::g_currentTickInScene > 0){
			::g_currentTickInScene--;
		}
		//call a method on the current scene that checks the tick count and sees whether a sound should
		//start or not
		::g_scenes[::g_masterSceneIndex]->CheckIfSoundStarts(::g_currentTickInScene, ::g_psystem);


		Sleep(30);
		::g_keydown = false;
	}

	//A   //Changing volume/pitch/etc whatever is selected down
	else if ((GetKeyState(0x41) < 0) && !::g_keydown) {
		::g_keydown = true;

		//if we are changing all the sounds opposed to just one
		if (::g_changingAllSounds) {
			switch (changingAttribute)
			{
				//changing volume down
			case eChangingAttribute::VOLUME:
				for (int sceneIndex = 0; sceneIndex < ::g_scenes.size(); sceneIndex++) {
					for (int soundIndex = 0; soundIndex < ::g_scenes[sceneIndex]->mediaObjects.size(); soundIndex++) {
						if (::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel) {
							float tempVolume = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->volume;
							if (tempVolume > 0.05) {
								::g_result = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel->setVolume(tempVolume - 0.1);
								::g_scenes[sceneIndex]->mediaObjects[soundIndex]->volume -= 0.1;
							}
						}
					}
				}
				break;
				//changing balance left
			case eChangingAttribute::BALANCE:
				for (int sceneIndex = 0; sceneIndex < ::g_scenes.size(); sceneIndex++) {
					for (int soundIndex = 0; soundIndex < ::g_scenes[sceneIndex]->mediaObjects.size(); soundIndex++) {
						if (::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel) {
							if (::g_scenes[sceneIndex]->mediaObjects[soundIndex]->pan > -1) {
								::g_result = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel->setPan(::g_scenes[sceneIndex]->mediaObjects[soundIndex]->pan - 0.1);
								::g_scenes[sceneIndex]->mediaObjects[soundIndex]->pan -= 0.1;
							}
						}
					}
				}
				break;
				//changing pitch down
			case eChangingAttribute::PITCH:
				for (int sceneIndex = 0; sceneIndex < ::g_scenes.size(); sceneIndex++) {
					for (int soundIndex = 0; soundIndex < ::g_scenes[sceneIndex]->mediaObjects.size(); soundIndex++) {
						if (::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel) {
							float tempPitch = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->pitch;
							if (tempPitch > 0) {
								::g_result = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel->setPitch(tempPitch-0.1);
								::g_scenes[sceneIndex]->mediaObjects[soundIndex]->pitch -= 0.1;
							}
						}
					}
				}
				break;
				//reducing playback speed
			case eChangingAttribute::PLAYBACKSPEED:
				for (int sceneIndex = 0; sceneIndex < ::g_scenes.size(); sceneIndex++) {
					for (int soundIndex = 0; soundIndex < ::g_scenes[sceneIndex]->mediaObjects.size(); soundIndex++) {
						if (::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel) {
							float tempPlayBackSpeed = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->playBackSpeed;
							if (tempPlayBackSpeed == 1.0) {
								::g_result = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel->setFrequency(44100 * 0.5);
								::g_scenes[sceneIndex]->mediaObjects[soundIndex]->playBackSpeed = 0.5;
								::g_scenes[sceneIndex]->mediaObjects[soundIndex]->frequency = 44100 * 0.5;
							}
							if (tempPlayBackSpeed == 2.0) {
								::g_result = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel->setFrequency(44100);
								::g_scenes[sceneIndex]->mediaObjects[soundIndex]->playBackSpeed = 1.0;
								::g_scenes[sceneIndex]->mediaObjects[soundIndex]->frequency = 44100;
							}
						}
					}
				}
				break;
				//reducing frequency
			case eChangingAttribute::FREQUENCY:
				for (int sceneIndex = 0; sceneIndex < ::g_scenes.size(); sceneIndex++) {
					for (int soundIndex = 0; soundIndex < ::g_scenes[sceneIndex]->mediaObjects.size(); soundIndex++) {
						if (::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel) {
							float tempFrequency = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->frequency;
							if (tempFrequency > -80000 ) {
								::g_result = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel->setFrequency(tempFrequency - 100);
								::g_scenes[sceneIndex]->mediaObjects[soundIndex]->frequency -= 100;
							}
						}
					}
				}
				break;
			}
		}
		else
		{
			switch (changingAttribute)
			{
				//changing volume down
			case eChangingAttribute::VOLUME:
				if (::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel) {
					float tempVolume = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->volume;
					if (tempVolume > 0.05) {
						::g_result = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel->setVolume(tempVolume - 0.1);
						::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->volume -= 0.1;
					}
				}
				break;
				//changing balance left
			case eChangingAttribute::BALANCE:
				if (::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel) {
					if (::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->pan > -1) {
						::g_result = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel->setPan(::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->pan - 0.1);
						::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->pan -= 0.1;
					}
				}
				break;
				//changing pitch down
			case eChangingAttribute::PITCH:
				if (::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel) {
					float tempPitch = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->pitch;
					if (tempPitch > 0) {
						::g_result = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel->setPitch(tempPitch - 0.1);
						::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->pitch -= 0.1;
					}
				}
				break;
				//reducing playback speed
			case eChangingAttribute::PLAYBACKSPEED:
			{
				float tempPlayBackSpeed = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->playBackSpeed;
				if (tempPlayBackSpeed == 1.0) {
					::g_result = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel->setFrequency(44100 * 0.5);
					::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->playBackSpeed = 0.5;
					::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->frequency = 44100 * 0.5;
				}
				if (tempPlayBackSpeed == 2.0) {
					::g_result = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel->setFrequency(44100);
					::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->playBackSpeed = 1.0;
					::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->frequency = 44100;
				}
			}
				break;
				//reducing frequency
			case eChangingAttribute::FREQUENCY:
				float tempFrequency = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->frequency;
				if (tempFrequency > -80000) {
					::g_result = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel->setFrequency(tempFrequency - 100);
					::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->frequency -= 100;
				}
				break;
			}
		}

		Sleep(200);
		::g_keydown = false;
	}
	//D //Changing volume/pitch/etc whatever is selected up
	else if ((GetKeyState(0x44) < 0) && !::g_keydown) {
		::g_keydown = true;
		//if changing all sounds or not
		if (::g_changingAllSounds) {
			switch (changingAttribute)
			{
				//changing the volume up
			case eChangingAttribute::VOLUME:
				for (int sceneIndex = 0; sceneIndex < ::g_scenes.size(); sceneIndex++) {
					for (int soundIndex = 0; soundIndex < ::g_scenes[sceneIndex]->mediaObjects.size(); soundIndex++) {
						if (::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel) {
							float tempVolume = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->volume;
							if (tempVolume < 1) {
								::g_result = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel->setVolume(tempVolume + 0.1);
								::g_scenes[sceneIndex]->mediaObjects[soundIndex]->volume += 0.1;
							}
						}
					}
				}
				break;
				//changing the balance right
			case eChangingAttribute::BALANCE:
				for (int sceneIndex = 0; sceneIndex < ::g_scenes.size(); sceneIndex++) {
					for (int soundIndex = 0; soundIndex < ::g_scenes[sceneIndex]->mediaObjects.size(); soundIndex++) {
						if (::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel) {
							if (::g_scenes[sceneIndex]->mediaObjects[soundIndex]->pan < 1) {
								::g_result = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel->setPan(::g_scenes[sceneIndex]->mediaObjects[soundIndex]->pan + 0.1);
								::g_scenes[sceneIndex]->mediaObjects[soundIndex]->pan += 0.1;
							}
						}
					}
				}
				break;
				//increasing the pitch
			case eChangingAttribute::PITCH:
				for (int sceneIndex = 0; sceneIndex < ::g_scenes.size(); sceneIndex++) {
					for (int soundIndex = 0; soundIndex < ::g_scenes[sceneIndex]->mediaObjects.size(); soundIndex++) {
						if (::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel) {
							float tempPitch = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->pitch;
							if (tempPitch < 3) {
								::g_result = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel->setPitch(tempPitch + 0.1);
								::g_scenes[sceneIndex]->mediaObjects[soundIndex]->pitch += 0.1;
							}
						}
					}
				}
				break;
				//increasing the playback speed
			case eChangingAttribute::PLAYBACKSPEED:
				for (int sceneIndex = 0; sceneIndex < ::g_scenes.size(); sceneIndex++) {
					for (int soundIndex = 0; soundIndex < ::g_scenes[sceneIndex]->mediaObjects.size(); soundIndex++) {
						if (::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel) {
							float tempPlayBackSpeed = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->playBackSpeed;
							if (tempPlayBackSpeed == 0.5) {
								::g_result = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel->setFrequency(44100);
								::g_scenes[sceneIndex]->mediaObjects[soundIndex]->playBackSpeed = 1.0f;
								::g_scenes[sceneIndex]->mediaObjects[soundIndex]->frequency = 44100;
							}
							if (tempPlayBackSpeed == 1.0) {
								::g_result = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel->setFrequency(44100*2);
								::g_scenes[sceneIndex]->mediaObjects[soundIndex]->playBackSpeed = 2.0;
								::g_scenes[sceneIndex]->mediaObjects[soundIndex]->frequency = 44100 * 2;
							}
						}
					}
				}
				break;
				//increasing the frequency
			case eChangingAttribute::FREQUENCY:
				for (int sceneIndex = 0; sceneIndex < ::g_scenes.size(); sceneIndex++) {
					for (int soundIndex = 0; soundIndex < ::g_scenes[sceneIndex]->mediaObjects.size(); soundIndex++) {
						if (::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel) {
							float tempFrequency = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->frequency;
							if (tempFrequency < 80000) {
								::g_result = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel->setFrequency(tempFrequency + 100);
								::g_scenes[sceneIndex]->mediaObjects[soundIndex]->frequency += 100;
							}
						}
					}
				}
				break;
			}
		}
		else //only changing one sound
		{
			switch (changingAttribute)
			{
			//increasing the volume
			case eChangingAttribute::VOLUME:
				if (::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel) {
					float tempVolume = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->volume;
					if (tempVolume < 1) {
						::g_result = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel->setVolume(tempVolume + 0.1);
						::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->volume += 0.1;
					}
				}
				break;
				//changing the balance right
			case eChangingAttribute::BALANCE:
				if (::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel) {
					if (::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->pan < 1) {
						::g_result = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel->setPan(::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->pan + 0.1);
						::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->pan += 0.1;
					}
				}
				break;
				//increasing the pitch
			case eChangingAttribute::PITCH:
				if (::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel) {
					float tempPitch = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->pitch;
					if (tempPitch < 3) {
						::g_result = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel->setPitch(tempPitch + 0.1);
						::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->pitch += 0.1;
					}
				}
				break;
				//increasing the playback speed
			case eChangingAttribute::PLAYBACKSPEED:
			{
				float tempPlayBackSpeed = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->playBackSpeed;
				if (tempPlayBackSpeed == 0.5) {
					::g_result = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel->setFrequency(44100);
					::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->playBackSpeed = 1.0f;
					::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->frequency = 44100;
				}
				if (tempPlayBackSpeed == 1.0) {
					::g_result = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel->setFrequency(44100 * 2);
					::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->playBackSpeed = 2.0;
					::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->frequency = 44100 * 2;
				}
			}
				break;
				//increasing the frequency
			case eChangingAttribute::FREQUENCY:
			{
				float tempFrequency = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->frequency;
				if (tempFrequency < 80000) {
					::g_result = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->channel->setFrequency(tempFrequency + 100);
					::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->frequency += 100;
				}
			}
				break;
			}
		}
		Sleep(200);
		::g_keydown = false;
	}

	//W //changing what type of thing we will be changing for the current sound, pushing it up
	else if ((GetKeyState(0x57) < 0) && !::g_keydown) {
		::g_keydown = true;
		//changing which attribute is being changed
		switch (changingAttribute)
		{
		case eChangingAttribute::VOLUME:
			changingAttribute = eChangingAttribute::BALANCE;
			break;
		case eChangingAttribute::BALANCE:
			changingAttribute = eChangingAttribute::PITCH;
			break;
		case eChangingAttribute::PITCH:
			changingAttribute = eChangingAttribute::PLAYBACKSPEED;
			break;
		case eChangingAttribute::PLAYBACKSPEED:
			changingAttribute = eChangingAttribute::FREQUENCY;
			break;
		case eChangingAttribute::FREQUENCY:
			changingAttribute = eChangingAttribute::VOLUME;
			break;
		}
		

		Sleep(200);
		::g_keydown = false;
	}
	//S //changing what type of thing we will be changing for the current sound, pushing it down
	else if ((GetKeyState(0x53) < 0) && !::g_keydown) {
		::g_keydown = true;
		//changing which attribute is being changed
		switch (changingAttribute)
		{
		case eChangingAttribute::VOLUME:
			changingAttribute = eChangingAttribute::FREQUENCY;
			break;
		case eChangingAttribute::BALANCE:
			changingAttribute = eChangingAttribute::VOLUME;
			break;
		case eChangingAttribute::PITCH:
			changingAttribute = eChangingAttribute::BALANCE;
			break;
		case eChangingAttribute::PLAYBACKSPEED:
			changingAttribute = eChangingAttribute::PITCH;
			break;
		case eChangingAttribute::FREQUENCY:
			changingAttribute = eChangingAttribute::PLAYBACKSPEED;
			break;
		}
		Sleep(200);
		::g_keydown = false;
	}

	//Q //The button that switches if changing attributes for all sounds, or just one
	else if ((GetKeyState(0x51) < 0) && !::g_keydown) {
		::g_keydown = true;
		
		::g_changingAllSounds = !::g_changingAllSounds;

		Sleep(200);
		::g_keydown = false;
	}

}

int main()
{

	init_fmod();

	//ask the user if they want compressed or uncompressed background sounds
	printf("Would you like compressed sounds y/n\n");
	string response;
	cin >> response;
	if (response == "y")
		::g_compressedSelected = true;

	bool loaded = LoadSoundsFromFile();


	if (loaded) {
		//start the first scene
		::g_scenes[::g_masterSceneIndex]->Startup();

		//calculate the total ticks for the all the scenesfor (int sceneIndex = 0; sceneIndex < scenes.size(); sceneIndex++) {
		for (int sceneIndex = 0; sceneIndex < ::g_scenes.size(); sceneIndex++) {
			::g_totalTicks += ::g_scenes[sceneIndex]->length;
		}

		string sceneShower = "";
		//create the string that shows where the scenes start and end 
		for (int sceneIndex = 0; sceneIndex < ::g_scenes.size(); sceneIndex++) {
			string sceneShowerTemp((float(::g_scenes[sceneIndex]->length) / float(::g_totalTicks)) * 49, '-');
			sceneShowerTemp.append("|");
			sceneShower.append(sceneShowerTemp);
		}

		//the variables needed to display all the information about the currently selected sound
		//declared here so they aren't declared everytime the main loop runs
		string soundName = "";
		FMOD_SOUND_TYPE soundType = FMOD_SOUND_TYPE_UNKNOWN;
		FMOD_SOUND_FORMAT soundFormat = FMOD_SOUND_FORMAT_NONE;
		unsigned int soundLength;

		//Information we are acquiring but not using
		int soundChannels;
		int soundBits;

		while (!::g_mis_esc)
		{
			//Needed for print_text
			start_text();
			handle_keyboard();


			//Get all the information for the currently selected sound
			MediaObject* tempMediaObject = ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound];
			soundName = tempMediaObject->fileName;

			//if the channel is working
			if (tempMediaObject->channel)
			{
				//if the sound if working
				if (tempMediaObject->sound)
				{
					//get the format and type
					::g_result = tempMediaObject->sound->getFormat(&soundType, &soundFormat, &soundChannels, &soundBits);
					checkForErrors();

					//get the length
					::g_result = tempMediaObject->sound->getLength(&soundLength, FMOD_TIMEUNIT_MS);
					checkForErrors();

				}

			}


			//Important to update msystem
			::g_result = ::g_psystem->update();
			checkForErrors();

			//calculate the progress
			string progressString((float(::g_currentTick + 1) / float(::g_totalTicks)) * 50, '|');
			string progressLeftString( (float(::g_totalTicks- ::g_currentTick)/float(::g_totalTicks)) * 50, '=');

			print_text("==============================================================");
			print_text("The Deserter: Scene %d : %s                                        ", ::g_masterSceneIndex + 1, ::g_scenes[::g_masterSceneIndex]->prompt.c_str());
			print_text("Progress:");
			print_text("%s:%s", progressString.c_str(), progressLeftString.c_str());
			print_text("%s", sceneShower.c_str());
			print_text("Information for the currently selected file");
			print_text("Sound File Name: %s              ", soundName.c_str());
			print_text("Sound Format: %s            ", convertSoundFormatCode(soundFormat).c_str());
			print_text("Sound Type: %s             ", convertSoundTypeCode(soundType).c_str());
			print_text("Sound Frequency: %.2f Hz           ", ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->frequency);
			print_text("Sound Pitch: %.2f         ", ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->pitch);

			print_text("Length of sound: %02d:%02d:%02.0f            ",
				(soundLength / 1000) / 60,
				((soundLength / 1000) % 60),
				(float(soundLength %1000) / 1000.0f) * 100.0f);
			print_text("Playback speed: %.1fx    ", ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->playBackSpeed);
			print_text("Sound Volume: %.2f     ", ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->volume);
			print_text("Sound Balance: %.1f         ", ::g_scenes[::g_currentSelectedSoundScene]->mediaObjects[::g_currentSelectedSound]->pan);
			print_text("==============================================================");
			print_text("Press the right arrow key to move the story forward");
			print_text("Press Esc to Exit");
			print_text("Use the up and down arrow keys to choose which sounds info is being displayed");
			switch (changingAttribute)
			{
			case eChangingAttribute::VOLUME:
				print_text("Use w and s to change which attribute of the sound is being changed. Currently changing: VOLUME     ");
				break;
			case eChangingAttribute::BALANCE:
				print_text("Use w and s to change which attribute of the sound is being changed. Currently changing: BALANCE      ");
				break;
			case eChangingAttribute::PITCH:
				print_text("Use w and s to change which attribute of the sound is being changed. Currently changing: PITCH         ");
				break;
			case eChangingAttribute::PLAYBACKSPEED:
				print_text("Use w and s to change which attribute of the sound is being changed. Currently changing: PLAYBACKSPEED    ");
				break;
			case eChangingAttribute::FREQUENCY:
				print_text("Use w and s to change which attribute of the sound is being changed. Currently changing: FREQUENCY    ");
				break;
			}
			print_text("Use a and d to alter the current attribute: ");
			print_text("Press q to go between changing a single sound or all sounds. Changing all sounds? %s", ::g_changingAllSounds ? "Yes" : "No ");
			//display all the sounds for the current scene
			print_text("Sounds in Scene");
			int mediaObjectsSize = ::g_scenes[::g_masterSceneIndex]->mediaObjects.size();
			for (int mediaObjectIndex = 0; mediaObjectIndex < mediaObjectsSize; mediaObjectIndex++)
			{
				print_text("fileName: %s                 ", ::g_scenes[::g_masterSceneIndex]->mediaObjects[mediaObjectIndex]->fileName.c_str());
			}

			//Needed for print_text
			end_text();

			Sleep(50);
		}
	}
	else
	{
		cout << "Failed to load from text file, please check README.txt for wanted formatting" << endl;
	}

	for (int sceneIndex = 0; sceneIndex < ::g_scenes.size(); sceneIndex++) {
		for (int soundIndex = 0; soundIndex < ::g_scenes[sceneIndex]->mediaObjects.size(); soundIndex++)
		{
			//Shutdown
			if (::g_scenes[sceneIndex]->mediaObjects[soundIndex]->sound) {
				::g_result = ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->sound->release();
				checkForErrors();
			}
			delete ::g_scenes[sceneIndex]->mediaObjects[soundIndex]->channel;
		}
	}

	if (::g_psystem) {
		::g_result = ::g_psystem->close();
		checkForErrors();
		::g_result = ::g_psystem->release();
		checkForErrors();
	}

	return 0;
}